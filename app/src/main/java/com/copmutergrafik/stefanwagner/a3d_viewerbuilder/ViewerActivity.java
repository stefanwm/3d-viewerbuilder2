package com.copmutergrafik.stefanwagner.a3d_viewerbuilder;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.opengl.GLSurfaceView;
import android.os.Environment;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.builder.TriangleElevation;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.mesh.HalfEdgeTriangleMesh;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.mesh.ObjReader;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.rendering.Shader;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.scenegraph.Camera;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.scenegraph.HalfEdgeMeshNodeSimple;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.scenegraph.INode;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.scenegraph.RootNode;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.utilities.ObjectCenter;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.utilities.ProgressDialogTaskRefresh;

import java.io.File;
import java.util.ArrayList;

/**
 * This class is the starting point of the app. Shader, Camera and Root node
 * are instantiated here.
 */

public class ViewerActivity extends Activity {

    private GLSurfaceView mGLView;
    private TriangleElevation te;

    private Shader.ShaderMode mode = Shader.ShaderMode.PHONG;

    private SeekBar meshSize;

    private float meshSizeValue;
    private boolean vertexLighting = true;
    private String storageDirectory = null;

    private ListView mDrawerList;
    private RelativeLayout mDrawerPane;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;

    private static ArrayList<INode> childList = new ArrayList<>();
    private ArrayList<NavItem> mNavItems = new ArrayList<NavItem>();

    public static RootNode root;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mGLView = (ViewerGLSurfaceView) findViewById(R.id.glSurfaceView);

        storageDirectory = Environment.getExternalStorageDirectory().getAbsolutePath();// this will get you internal directory path

        Shader shader = new Shader(mode, storageDirectory);
        Camera camera = new Camera();

        this.root = new RootNode(shader, camera);
        root.setAnimated(true);

        for (INode childNode : childList) {
            root.addChild(childNode);
        }
        setContentView(R.layout.activity_main);

        /************************* View load finished *********************/

        //Fill navItems with file names
        getMeshFiles(mNavItems);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);

        // Populate the Navigtion Drawer with options
        mDrawerPane = (RelativeLayout) findViewById(R.id.drawerPane);
        mDrawerList = (ListView) findViewById(R.id.navList);
        DrawerListAdapter adapter = new DrawerListAdapter(this, mNavItems);
        mDrawerList.setAdapter(adapter);

        // Drawer Item click listeners
        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectItemFromDrawer(position);
            }

            private void selectItemFromDrawer(int position) {
                loadObject(mNavItems.get(position).mTitle);
            }
        });

        setupSeekBars();
        setupToggleButton();
    }


    /**
     * Add child nodes to childList for orientation change
     *
     * @param childNode Child node to be saved
     */
    public static void addToChildren(INode childNode) {
        if (!childList.isEmpty()) {
            root.removeChild(childList.get(0));
            childList.clear();
        }

        root.addChild(childNode);
        childList.add(childNode);
    }

    /**
     * Add multiple children to load obj file with multiple objects
     *
     * @param childNode Child node to be saved
     */
    public static void addToMultipleChildren(INode childNode) {
        root.addChild(childNode);
        childList.add(childNode);
    }

    private void setupSeekBars() {
        meshSize = (SeekBar) findViewById(R.id.meshSize);

        meshSize.setMax(10);
        meshSize.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                meshSizeValue = ((float) (progress)) / 10.0f;
                ((TextView) findViewById(R.id.meshSizeText)).setText("Mesh Size: " + String.valueOf(meshSizeValue));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
    }

    private void setupToggleButton() {
        ToggleButton toggle = (ToggleButton) findViewById(R.id.toggleButton);
        toggle.setChecked(true);
        toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    vertexLighting = true;
                } else {
                    vertexLighting = false;
                }
            }
        });

        ToggleButton toggleShading = (ToggleButton) findViewById(R.id.shadingButton);
        toggleShading.setChecked(true);
        toggleShading.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    root.getShader().setShader(Shader.ShaderMode.PHONG);
                } else {
                    root.getShader().setShader(Shader.ShaderMode.GOURAUD);
                }
                Shader.setShaderModified(true);
            }
        });
    }


    @Override
    protected void onResume() {
        // The activity must call the GL surface view's onResume() on activity onResume().
        super.onResume();
        if (mGLView != null) {
            mGLView.onResume();
        }
    }

    @Override
    protected void onPause() {
        // The activity must call the GL surface view's onPause() on activity onPause().
        super.onPause();
        if (mGLView != null) {
            mGLView.onPause();
        }
    }

    /**
     * Fill navigation item of drawer with mesh OBJ-files
     *
     * @param navItems
     */
    private void getMeshFiles(ArrayList<NavItem> navItems) {
        File sdCardFile = new File(storageDirectory + "/meshes/");

        for (int i = 0; i < sdCardFile.listFiles().length; i++) {
            navItems.add(new NavItem(sdCardFile.listFiles()[i].getName()));
        }
    }

    /**
     * Start intent that opens canvas for drawing
     *
     * @param view
     */
    public void openCanvas(View view) {
        Intent intent = new Intent(this, CanvasActivity.class);
        startActivityForResult(intent, 1);
    }

    /**
     * Reload object after changing parameters
     *
     * @param view
     */
    public void refresh(View view) {
        new ProgressDialogTaskRefresh(this, te, meshSizeValue, vertexLighting).execute();
    }

    /**
     * Read OBJ-File and set object into scene
     *
     * @param filename
     */
    public void loadObject(String filename) {
        HalfEdgeTriangleMesh mesh = new HalfEdgeTriangleMesh(true);

        ObjReader objReader = new ObjReader(storageDirectory);
        objReader.read(filename, mesh);

        ObjectCenter.centerObject(mesh);

        HalfEdgeMeshNodeSimple node = new HalfEdgeMeshNodeSimple(mesh, true);
        addToChildren(node);

        mDrawerLayout.closeDrawer(Gravity.LEFT);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                Bundle resultBundle = data.getExtras();
                te = (TriangleElevation) resultBundle.getSerializable("0");
            }
        }
    }

    class NavItem {
        String mTitle;

        public NavItem(String title) {
            mTitle = title;
        }
    }

    class DrawerListAdapter extends BaseAdapter {

        Context mContext;
        ArrayList<NavItem> mNavItems;

        public DrawerListAdapter(Context context, ArrayList<NavItem> navItems) {
            mContext = context;
            mNavItems = navItems;
        }

        @Override
        public int getCount() {
            return mNavItems.size();
        }

        @Override
        public Object getItem(int position) {
            return mNavItems.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view;

            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.drawer_item, null);
            } else {
                view = convertView;
            }

            TextView titleView = (TextView) view.findViewById(R.id.title);
            titleView.setText(mNavItems.get(position).mTitle);

            return view;
        }
    }
}
