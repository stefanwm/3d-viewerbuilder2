package com.copmutergrafik.stefanwagner.a3d_viewerbuilder.scenegraph;

import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.math.MathHelpers.Matrix;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.math.MathHelpers.Vector;

/**
 * Created by Stefan Wagner on 06.03.2017.
 */

public class TranslationNode extends InnerNode {
    /**
     * Translation matrix (model matrix)
     */
    private final Matrix translation;

    public TranslationNode(Vector translation) {
        this.translation = Matrix.createTranslationGl(translation);
    }

    public void traverse(RenderMode mode, Matrix modelMatrix) {
        super.traverse(mode, translation.multiply(modelMatrix));
    }
}
