package com.copmutergrafik.stefanwagner.a3d_viewerbuilder.rendering;

import android.graphics.Bitmap;

import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.math.MathHelpers.Vector;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Stefan on 04.02.2017.
 * This class represents a pixel in the canvas used to draw the object's contour
 */

public class Pixel {
    private int x;
    private int y;
    private static ArrayList<Pixel> drawnPoints = new ArrayList<>();

    public Pixel(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Vector getPixelVector() {
        Vector pixelVector = new Vector(2);
        pixelVector.set(0, x);
        pixelVector.set(1, y);
        return pixelVector;
    }

    /**
     * Calculate normed contour points to generate 3D-Object
     *
     * @return Normed contour points of object
     */
    public static ArrayList<Vector> getNormedDrawnPoints() {
        ArrayList<Vector> normedContourPoints = new ArrayList<>();
        float magnitude = 0;
        float max = 0;
        for (Pixel points : drawnPoints) {
            magnitude = (float) points.getPixelVector().getNorm();
            if (magnitude > max) {
                max = magnitude;
            }
        }

        int i = 0;
        drawnPoints.remove(0);
        for (Pixel points : drawnPoints) {
            Vector normedPoint = points.getPixelVector().multiply(1 / max);
            Vector normedContourPoint = new Vector(normedPoint.x(), normedPoint.y(), 0);

            normedContourPoint.setId(String.valueOf(i));
            normedContourPoints.add(normedContourPoint);

            i++;
        }
        return normedContourPoints;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public static ArrayList<Pixel> getDrawnPoints() {
        return drawnPoints;
    }

    public static void clear() {
        drawnPoints.clear();
    }
}
