package com.copmutergrafik.stefanwagner.a3d_viewerbuilder.scenegraph;

import android.opengl.GLES20;

import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.halfedge.HalfEdge;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.halfedge.HalfEdgeTriangle;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.halfedge.HalfEdgeVertex;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.mesh.IHalfEdgeTriangleMesh;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.rendering.RenderVertex;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.rendering.VertexBufferObject;

import java.util.ArrayList;
import java.util.List;

import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.math.MathHelpers.Vector;

/**
 * Created by Stefan on 26.02.2017.
 */

public class HalfEdgeMeshNodeSimple extends LeafNode {
    private VertexBufferObject vboLines = new VertexBufferObject();
    private VertexBufferObject vbo = new VertexBufferObject();
    private boolean vertexNormalsEnabled;

    public HalfEdgeMeshNodeSimple(IHalfEdgeTriangleMesh mesh, boolean vertex) {
        this.vertexNormalsEnabled = vertex;
        createVbo(mesh);
    }

    private void createVbo(IHalfEdgeTriangleMesh mesh) {
        List<RenderVertex> renderVertices = new ArrayList<RenderVertex>();
        List<RenderVertex> renderVerticesLines = new ArrayList<RenderVertex>();

        Vector normal1 = null;
        Vector normal2 = null;
        Vector normal3 = null;

        for (int i = 0; i < mesh.getNumberOfTriangles(); i++) {
            HalfEdgeTriangle facet = mesh.getHalfEdgeTriangle(i);

            /* Get half edges */
            HalfEdge he1 = facet.getHalfEdge();
            HalfEdge he2 = he1.getNext();
            HalfEdge he3 = he2.getNext();

            /* Get vertices */
            HalfEdgeVertex hv1 = he1.getStartVertex();
            HalfEdgeVertex hv2 = he2.getStartVertex();
            HalfEdgeVertex hv3 = he3.getStartVertex();

            /* get normals */
            if (vertexNormalsEnabled) {
                normal1 = hv1.getNormal();
                normal2 = hv2.getNormal();
                normal3 = hv3.getNormal();
            } else if(!vertexNormalsEnabled && facet.getNormal() != null) {
                normal1 = facet.getNormal();
                normal2 = facet.getNormal();
                normal3 = facet.getNormal();
            }

            Vector color = new Vector(0.149, 0.949, 0.945, 1);

            /* add render vertices */
            renderVertices.add(new RenderVertex(hv1.getPosition(), normal1, color));
            renderVertices.add(new RenderVertex(hv2.getPosition(), normal2, color));
            renderVertices.add(new RenderVertex(hv3.getPosition(), normal3, color));

        }
        vbo.Setup(renderVertices, GLES20.GL_TRIANGLES);

    }

    private void setLine(List<RenderVertex> renderVerticesLines, Vector normal, Vector v0, Vector v1) {
        renderVerticesLines.add(new RenderVertex(v0, normal, new Vector(0.5, 1.0, 0.5, 1)));
        renderVerticesLines.add(new RenderVertex(v1, normal, new Vector(0.5, 1.0, 0.5, 1)));
    }

    @Override
    public void drawGL() {
        vbo.draw();
    }
}
