package com.copmutergrafik.stefanwagner.a3d_viewerbuilder.scenegraph;

import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.math.MathHelpers.Matrix;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.math.MathHelpers.Vector;

/**
 * Created by Stefan Wagner on 30.01.2017.
 */

public class Camera {

    public static float[] mModelMatrix = new float[16];

    public static float[] mViewMatrix = new float[16];

    public static float[] mProjectionMatrix = new float[16];

    public static float[] mMVPMatrix = new float[16];

    /**
     * Near clipping distance.
     */
    private double zNear;

    private float angle;
    /**
     * Far clipping distance.
     */
    private double zFar;

    /**
     * Opening angle of the camera (in degrees).
     */
    private double fovy;

    /**
     * Position of the eye.
     */
    private static Vector eye;

    /**
     * Reference point of the camera (look at)
     */
    private static Vector ref;

    /**
     * Up-vector of the camera.
     */
    private static Vector up;

    /**
     * Window aspect ratio.
     */
    private float aspectRatio;

    /**
     * OpenGL view matrix.
     */
    private Matrix viewMatrix;

    public Camera() {
        eye = new Vector(0, 0, 5);
        ref = new Vector(0, 0, 0);
        up = new Vector(0, 1.0, 0);
        aspectRatio = 1;
        fovy = 50.0f;
        zNear = 0.1f;
        zFar = 20.0f;
        mViewMatrix = makeLookUpMatrix();
    }

    /**
     * Create an OpenGL lookup-matrix.
     */
    private float[] makeLookUpMatrix() {
        Vector y = up.getNormalized();
        Vector z = eye.subtract(ref).getNormalized();
        Vector x = y.cross(z).getNormalized();
        y = z.cross(x).getNormalized();
        Matrix R = new Matrix(x.x(), y.x(), z.x(), 0, x.y(), y.y(), z.y(), 0, x.z(),
                y.z(), z.z(), 0, 0, 0, 0, 1);
        Matrix T = new Matrix(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, -eye.x(),
                -eye.y(), -eye.z(), 1);

        Matrix multiplicationResult = T.multiply(R);
        float[] result = {
                (float) multiplicationResult.get(1, 0),
                (float) multiplicationResult.get(0, 1),
                (float) multiplicationResult.get(0, 2),
                (float) multiplicationResult.get(0, 3),
                (float) multiplicationResult.get(1, 0),
                (float) multiplicationResult.get(1, 1),
                (float) multiplicationResult.get(1, 2),
                (float) multiplicationResult.get(1, 3),
                (float) multiplicationResult.get(2, 0),
                (float) multiplicationResult.get(2, 1),
                (float) multiplicationResult.get(2, 2),
                (float) multiplicationResult.get(2, 3),
                (float) multiplicationResult.get(3, 0),
                (float) multiplicationResult.get(3, 1),
                (float) multiplicationResult.get(3, 2),
                (float) multiplicationResult.get(3, 3),
        };
        return result;
    }

    public static void setupLookAtMatrix(float eyeZ) {
        android.opengl.Matrix.setLookAtM(
                mViewMatrix,
                0,
                (float) eye.x(),
                (float) eye.y(),
                (float) eyeZ,
                (float) ref.x(),
                (float) ref.y(),
                (float) ref.z(),
                (float) up.x(),
                (float) up.y(),
                (float) up.z());
    }

    public void setupLookAtMatrix() {
        android.opengl.Matrix.setLookAtM(
                mViewMatrix,
                0,
                (float) eye.x(),
                (float) eye.y(),
                (float) eye.z(),
                (float) ref.x(),
                (float) ref.y(),
                (float) ref.z(),
                (float) up.x(),
                (float) up.y(),
                (float) up.z());
    }

    public void setupProjectionMatrix(int width, int height) {
        float aspectRatio = ((float) width / height);

        android.opengl.Matrix.perspectiveM(
                mProjectionMatrix,
                0,
                (float) fovy,
                (float) aspectRatio,
                (float) zNear,
                (float) zFar);
    }

    /**
     * Update the camera eye point by rotation angles.
     */
    public void updateExtrinsicCameraParams(float alpha, float beta) {
        Vector dir = eye.subtract(ref);
        // Rotate around up-vector
        eye = Matrix.getRotationMatrix3(up, alpha).multiply(dir).add(ref);
        // Rotate around side-vector
        dir = eye.subtract(ref);
        Vector side = dir.cross(up);
        side.normalize();
        eye = Matrix.getRotationMatrix3(side, -beta).multiply(dir).add(ref);
        // Fix up-vector
        dir = ref.subtract(eye);
        side = dir.cross(up);
        side.normalize();
        up = side.cross(dir);
        up.normalize();
        // Update LookAt
        setupLookAtMatrix();
    }

    /**
     * Zoom in/out.
     */
    public void zoom(float factor) {
        Vector dir = ref.subtract(eye);
        eye = eye.add(dir.multiply(0.02f * factor));
        mViewMatrix = makeLookUpMatrix();
    }

    public static Vector getEye() {
        return eye;
    }

    public Vector getRef() {
        return ref;
    }

    public Vector getUp() {
        return up;
    }

    public void setEye(Vector e) {
        eye = new Vector(e);
    }

    public void setRef(Vector e) {
        ref = new Vector(e);
    }

    public void setUp(Vector e) {
        up = new Vector(e);
    }

    public double getAspectRatio() {
        return aspectRatio;
    }

    public void setAspectRatio(float aspectRatio) {
        this.aspectRatio = aspectRatio;
    }

    public Matrix getViewMatrix() {
        return viewMatrix;
    }

    public double getFovy() {
        return fovy;
    }

    public double getZNear() {
        return zNear;
    }

    public double getZFar() {
        return zFar;
    }

    public float[] getmModelMatrix() {
        return mModelMatrix;
    }

    public float[] getmProjectionMatrix() {
        return mProjectionMatrix;
    }

    public float[] getmMVPMatrix() {
        return mMVPMatrix;
    }

    public static float[] getmViewMatrix() {
        return mViewMatrix;
    }

    public float getAngle() {
        return angle;
    }

    public void setAngle(float angle) {
        this.angle = angle;
    }

    @Override
    public String toString() {
        return "eye: " + eye + "\nref: " + ref + "\nup: " + up;
    }
}
