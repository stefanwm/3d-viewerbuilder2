package com.copmutergrafik.stefanwagner.a3d_viewerbuilder.mesh;

import android.util.Log;

import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.halfedge.HalfEdge;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.halfedge.HalfEdgeTriangle;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.halfedge.HalfEdgeVertex;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.math.MathHelpers.Vector;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.math.MathHelpers.VectorTriangle;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Stefan on 02.02.2017.
 */

public class HalfEdgeTriangleMesh implements IHalfEdgeTriangleMesh {
    private String id = "0";
    private ArrayList<HalfEdgeVertex> halfEdgeVertices;
    private ArrayList<HalfEdgeTriangle> facetes;
    private ArrayList<HalfEdge> halfEdges;
    private HashMap<String, HalfEdge> halfEdgeAndVertexPairMap;
    private HashMap<HalfEdgeVertex, HalfEdge> halfEdgeAndVertexMap;

    private boolean duplicate12 = false;
    private boolean duplicate23 = false;
    private boolean duplicate31 = false;

    //DEBUG
    boolean drawDifferentColor;
    String testId = "-1";

    public void setTestId(String id) {
        testId = id;
    }
    //DEBUG

    public HalfEdgeTriangleMesh(boolean b) {
        halfEdgeVertices = new ArrayList<>();
        facetes = new ArrayList<>();
        halfEdges = new ArrayList<>();
        halfEdgeAndVertexPairMap = new HashMap<String, HalfEdge>();
        halfEdgeAndVertexMap = new HashMap<>();
        drawDifferentColor = b;
    }

    @Override
    public int addVertex(Vector position) {
        halfEdgeVertices.add(new HalfEdgeVertex(position, id));
        id = String.valueOf(Integer.parseInt(id) + 1);
        return halfEdgeVertices.size() - 1;
    }

    public int addVertex(Vector position, int id) {
        halfEdgeVertices.add(new HalfEdgeVertex(position, String.valueOf(id)));
        return halfEdgeVertices.size() - 1;
    }

    @Override
    public void addTriangle(int vertexIndex1, int vertexIndex2, int vertexIndex3) {
        HalfEdgeTriangle currentHTriangle = new HalfEdgeTriangle();
        if (drawDifferentColor) {
            currentHTriangle.setId(testId);
        }

        HalfEdge halfEdge1 = new HalfEdge();
        HalfEdge halfEdge2 = new HalfEdge();
        HalfEdge halfEdge3 = new HalfEdge();

        Log.e("13 All", halfEdgeVertices.get(vertexIndex1).getId() + "|" + halfEdgeVertices.get(vertexIndex2).getId());
        Log.e("23 All", halfEdgeVertices.get(vertexIndex2).getId() + "|" + halfEdgeVertices.get(vertexIndex3).getId());
        Log.e("31 All", halfEdgeVertices.get(vertexIndex3).getId() + "|" + halfEdgeVertices.get(vertexIndex1).getId());
        Log.e("All", String.valueOf(vertexIndex1) + "  " + String.valueOf(vertexIndex2) + "   " + String.valueOf(vertexIndex3));

        if (halfEdgeAndVertexPairMap.containsKey(halfEdgeVertices.get(vertexIndex1).getId() + "|" + halfEdgeVertices.get(vertexIndex2).getId())) {
            Log.e("12 Single", halfEdgeVertices.get(vertexIndex1).getId() + "|" + halfEdgeVertices.get(vertexIndex2).getId());
            duplicate12 = true;
        } else if (halfEdgeAndVertexPairMap.containsKey(halfEdgeVertices.get(vertexIndex2).getId() + "|" + halfEdgeVertices.get(vertexIndex3).getId())) {
            Log.e("23 Single", halfEdgeVertices.get(vertexIndex2).getId() + "|" + halfEdgeVertices.get(vertexIndex3).getId());
            Log.e("23 Single", String.valueOf(vertexIndex1) + String.valueOf(vertexIndex2) + String.valueOf(vertexIndex3));
            duplicate23 = true;
        } else if (halfEdgeAndVertexPairMap.containsKey(halfEdgeVertices.get(vertexIndex3).getId() + "|" + halfEdgeVertices.get(vertexIndex1).getId())) {
            Log.e("31 Single", halfEdgeVertices.get(vertexIndex3).getId() + "|" + halfEdgeVertices.get(vertexIndex1).getId());
            duplicate31 = true;
        }


        if (!duplicate23 && !duplicate12 && !duplicate31) {
            halfEdgeAndVertexPairMap.put(halfEdgeVertices.get(vertexIndex1).getId() + "|" + halfEdgeVertices.get(vertexIndex2).getId(), halfEdge1);
            halfEdgeAndVertexPairMap.put(halfEdgeVertices.get(vertexIndex2).getId() + "|" + halfEdgeVertices.get(vertexIndex3).getId(), halfEdge2);
            halfEdgeAndVertexPairMap.put(halfEdgeVertices.get(vertexIndex3).getId() + "|" + halfEdgeVertices.get(vertexIndex1).getId(), halfEdge3);

            halfEdgeAndVertexMap.put(halfEdgeVertices.get(vertexIndex1), halfEdge1);
            halfEdgeAndVertexMap.put(halfEdgeVertices.get(vertexIndex2), halfEdge2);
            halfEdgeAndVertexMap.put(halfEdgeVertices.get(vertexIndex3), halfEdge3);

            halfEdge1.setStartVertex(halfEdgeVertices.get(vertexIndex1));
            halfEdge1.setEndVertex(halfEdgeVertices.get(vertexIndex2));
            halfEdge2.setStartVertex(halfEdgeVertices.get(vertexIndex2));
            halfEdge2.setEndVertex(halfEdgeVertices.get(vertexIndex3));
            halfEdge3.setStartVertex(halfEdgeVertices.get(vertexIndex3));
            halfEdge3.setEndVertex(halfEdgeVertices.get(vertexIndex1));

            halfEdge1.setNext(halfEdge2);
            halfEdge2.setNext(halfEdge3);
            halfEdge3.setNext(halfEdge1);

            halfEdge1.setFacet(currentHTriangle);
            halfEdge2.setFacet(currentHTriangle);
            halfEdge3.setFacet(currentHTriangle);

            currentHTriangle.setHalfEdge(halfEdge1);

            facetes.add(currentHTriangle);

            halfEdges.add(halfEdge1);
            halfEdges.add(halfEdge2);
            halfEdges.add(halfEdge3);

        }
        duplicate23 = false;
        duplicate12 = false;
        duplicate31 = false;
    }

    @Override
    public void addTriangle(int vertexIndex1, int vertexIndex2, int vertexIndex3,
                            int texCoordIndex1,
                            int texCoordIndex2, int texCoordIndex3) {
        addTriangle(vertexIndex1, vertexIndex2, vertexIndex3);
    }

    /**
     * Assign half edge to each available vertex.
     */
    private void buildHalfEdge() {
        for (HalfEdgeVertex vertex : halfEdgeVertices) {
            vertex.setHalfEgde(halfEdgeAndVertexMap.get(vertex));
        }
        setOppositeHalfEdges();
    }

    /**
     * For each half edge set opposite half edge if available. Else set opposite null.
     */
    private void setOppositeHalfEdges() {
        for (HalfEdge currentHalfEdge : halfEdges) {
            currentHalfEdge.setOpposite(halfEdgeAndVertexPairMap.get(currentHalfEdge.getEndVertex().getId() + "|" + currentHalfEdge.getStartVertex().getId()));
        }
    }

    @Override
    public void clear() {
        halfEdges.clear();
        halfEdgeVertices.clear();
        facetes.clear();
    }

    @Override
    public int getNumberOfTriangles() {
        return facetes.size();
    }

    @Override
    public int getNumberOfVertices() {
        return halfEdgeVertices.size();
    }

    @Override
    public HalfEdgeVertex getHalfEdgeVertex(int index) {
        return halfEdgeVertices.get(index);
    }

    @Override
    public Vertex getVertex(int index) {
        return null;
    }

    @Override
    public HalfEdgeTriangle getHalfEdgeTriangle(int index) {
        return facetes.get(index);
    }

    @Override
    public Triangle getTriangle(int triangleIndex) {
        return null;
    }

    public String getId() {
        id = String.valueOf(Integer.parseInt(id) + 1);
        return id;
    }

    @Override
    public Vector getTextureCoordinate(int index) {
        // TODO Auto-generated method stub
        return null;
    }

    public ArrayList<HalfEdge> getHalfEdges() {
        return halfEdges;
    }

    @Override
    public ArrayList<HalfEdgeTriangle> getFacetes() {
        return facetes;
    }

    @Override
    public ArrayList<HalfEdgeVertex> getHalfEdgeVertices() {
        return halfEdgeVertices;
    }


    @Override
    public void computeTriangleNormals() {
        buildHalfEdge();
        computeNormals();
    }

    public void computeTriangleNormalsAndJoinHalves() {
        buildHalfEdge();
        joinHalves();
        computeNormals();
    }

    /**
     * Get front facing half and back facing half of object and join them together
     * by adding opposites to half edge data structure
     */
    private void joinHalves() {
        ArrayList<HalfEdge> halfEdgesWithoutOpposite = new ArrayList<>();
        for (int i = 0; i < halfEdges.size(); i++) {
            int idStart = Integer.parseInt(halfEdges.get(i).getStartVertex().getId());
            int idEnd = Integer.parseInt(halfEdges.get(i).getEndVertex().getId());
            if (halfEdges.get(i).getOpposite() == null && Math.abs(idStart - idEnd) == 1) {
                halfEdgesWithoutOpposite.add(halfEdges.get(i));
            }
        }

        for (int i = 0; i < halfEdgesWithoutOpposite.size(); i++) {
            String newId = String.valueOf(Integer.parseInt(halfEdgesWithoutOpposite.get(i).getStartVertex().getId()) + (halfEdgeVertices.size() / 2));
            HalfEdge oppositeHalfEdge = halfEdgeAndVertexPairMap.get(newId + "|" + String.valueOf(Integer.parseInt(newId) + 1));


            if (oppositeHalfEdge != null) {
                halfEdgesWithoutOpposite.get(i).setOpposite(oppositeHalfEdge);
                oppositeHalfEdge.setOpposite(halfEdgesWithoutOpposite.get(i));
            }
        }
    }

    /**
     * Move vertices to center point
     * @param alpha Factor by which to move vertices closer to center point
     */
    public void moveVerticesToCenter(float alpha) {
        for (HalfEdgeVertex vertex : halfEdgeVertices) {
            float x = (float) vertex.getPosition().x();
            float y = (float) vertex.getPosition().y();
            float z = (float) vertex.getPosition().z();
            if (vertex.getCentroid().x() != 0 && vertex.getCentroid().y() != 0 && vertex.getCentroid().z() != 0) {
                vertex.getPosition().set(0, (alpha * x) + ((1 - alpha) * vertex.getCentroid().x()));
                vertex.getPosition().set(1, (alpha * y) + ((1 - alpha) * vertex.getCentroid().y()));
                vertex.getPosition().set(2, (alpha * z) + ((1 - alpha) * vertex.getCentroid().z()));
            }
        }
    }

    /**
     * Calculate center points of each half edge vertex
     * @param iterations Number of times to calculate center points
     */
    public void calculateCentroids(int iterations) {
        for (int i = 0; i < iterations; i++) {
            for (HalfEdgeVertex vertex : halfEdgeVertices) {
                Vector centroid = new Vector(0, 0, 0);
                if (vertex.getHalfEdge() != null) {
                    int counter = 0;
                    HalfEdge start = vertex.getHalfEdge(); //Starting half edge.
                    HalfEdge tmp = start; //Current half edge

                    do {
                        if (tmp.getOpposite() != null) {
                            centroid = centroid.add(tmp.getNext().getStartVertex().getPosition());
                            counter++;
                            tmp = tmp.getOpposite();
                        }
                        tmp = tmp.getNext();
                    } while (tmp != start);
                    vertex.setCentroid(centroid.multiply(1.0 / counter));
                }
            }
        }
    }

    private void computeNormals() {
        for (HalfEdgeVertex vertex : halfEdgeVertices) {
            Vector normalValue = new Vector(0, 0, 0); //Sum of incident facet normals for vertex
            if (vertex.getHalfEdge() != null) {
                int counter = 0;
                HalfEdge start = vertex.getHalfEdge(); //Starting half edge.
                HalfEdge tmp = start; //Current half edge

                do {
                /*
                 * Only get normal of facet once for half edge with opposite half edge
				 */
                    if (tmp.getOpposite() != null) {
                        Vector facetNormal = this.normalForFacet(tmp.getStartVertex(),
                                tmp.getNext().getStartVertex(),
                                tmp.getNext().getNext().getStartVertex());
                        tmp.getFacet().setNormal(facetNormal); //Set facet(triangle) normal
                        normalValue = normalValue.add(facetNormal.multiply(tmp.getFacet().getArea()));
                        counter++;
                        tmp = tmp.getOpposite();
                    }
                    tmp = tmp.getNext();


                } while (tmp != start);
                //Set vertex normal out of sum of its incident facets
                vertex.setNormal(normalValue.getNormalized());
            }
        }
    }

    public void assignTriangleNormals(HalfEdgeTriangleMesh mesh) {
       for(HalfEdgeTriangle facet : facetes){
           facet.getHalfEdge().getStartVertex().setNormal(mesh.getHalfEdgeVertices().get(Integer.parseInt( facet.getHalfEdge().getStartVertex().getId()) + 30).getNormal());
           facet.getHalfEdge().getNext().getStartVertex().setNormal(mesh.getHalfEdgeVertices().get(Integer.parseInt( facet.getHalfEdge().getNext().getStartVertex().getId()) + 30).getNormal());
           facet.getHalfEdge().getNext().getNext().getStartVertex().setNormal(mesh.getHalfEdgeVertices().get(Integer.parseInt( facet.getHalfEdge().getNext().getNext().getStartVertex().getId()) + 30).getNormal());
       }
    }

    /**
     * Calculate normal for facet given by each half edge vertex
     *
     * @param v1 vertex 1
     * @param v2 vertex 2
     * @param v3 vertex 3
     * @return Normalized normal for facet
     */
    public Vector normalForFacet(HalfEdgeVertex v1, HalfEdgeVertex v2, HalfEdgeVertex v3) {
        Vector v = v2.getPosition().subtract(v1.getPosition());
        Vector w = v3.getPosition().subtract(v1.getPosition());

        return v.cross(w).getNormalized();
    }

    @Override
    public void createShadowPolygons(Vector lightPosition, float extend, ITriangleMesh
            shadowPolygonMesh) {
        // TODO Auto-generated method stub

    }


}
