package com.copmutergrafik.stefanwagner.a3d_viewerbuilder.mesh;

import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.math.MathHelpers.Vector;

/**
 * Created by Stefan on 02.02.2017.
 */

public class Vertex {
    /**
     * Vertex position in 3-space.
     */
    private Vector position = new Vector(0, 0, 0);

    /**
     * Vertex normal in 3-space.
     */
    private Vector normal = new Vector(0, 1, 0);

    public Vertex(Vector position) {
        this(position, new Vector(0, 1, 0));
    }

    public Vertex(Vector position, Vector normal) {
        this.position.copy(position);
        this.normal.copy(normal);
    }

    public Vector getPosition() {
        return position;
    }

    public Vector getNormal() {
        return normal;
    }
}
