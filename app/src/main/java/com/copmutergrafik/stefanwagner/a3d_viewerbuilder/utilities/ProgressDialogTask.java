package com.copmutergrafik.stefanwagner.a3d_viewerbuilder.utilities;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.ViewerActivity;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.builder.MeshGenerator;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.builder.SimplifyMesh;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.builder.Skeleton;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.builder.TriangleElevation;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.builder.Triangulation;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.math.MathHelpers.Vector;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.mesh.HalfEdgeTriangleMesh;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.rendering.Pixel;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.scenegraph.Camera;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.scenegraph.ContourTestNode;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.scenegraph.HalfEdgeTriangleMeshNode;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.scenegraph.TranslationNode;

/**
 * Created by Stefan on 07.02.2017.
 */

public class ProgressDialogTask extends AsyncTask<Void, String, Void> {
    private ProgressDialog dialog;
    private TriangleElevation te;

    public ProgressDialogTask(Context context) {
        dialog = new ProgressDialog(context);
    }

    @Override
    protected void onPreExecute() {
        dialog.show();
    }

    @Override
    protected void onPostExecute(Void result) {
        if (dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    @Override
    protected Void doInBackground(Void... params) {
        publishProgress("Starting Triangulation...");
        Triangulation triangulation = new Triangulation(Pixel.getNormedDrawnPoints());

        publishProgress("Performing Triangulation...");
        triangulation.triangulate();

        HalfEdgeTriangleMesh tMesh = new HalfEdgeTriangleMesh(false);
        publishProgress("Adding vertices to half edge...");
        for (int i = 0; i < triangulation.getOriginalVertices().size(); i++) {
            tMesh.addVertex(triangulation.getOriginalVertices().get(i));
        }
        publishProgress("Adding triangle indeces to half edge...");
        for (int i = 0; i < triangulation.getTriangles().size(); i++) {
            tMesh.addTriangle(triangulation.getTriangles().get(i).getTriangle()[0].getId(),
                    triangulation.getTriangles().get(i).getTriangle()[1].getId(),
                    triangulation.getTriangles().get(i).getTriangle()[2].getId());
        }
        publishProgress("Computing triangle normals 1...");
        tMesh.computeTriangleNormals();

        publishProgress("Simplifying Mesh...");
        SimplifyMesh sm = new SimplifyMesh(tMesh);

        publishProgress("Creating axis...");
        Skeleton sp = new Skeleton(sm.getInnerHalfEdges(), tMesh);
        sp.elevateSpine();

        publishProgress("Elevating axis...");
        te = new TriangleElevation(sp);

        publishProgress("Generating mesh...");
        HalfEdgeTriangleMesh tMesh2 = new HalfEdgeTriangleMesh(false);
        MeshGenerator meshGenerator = new MeshGenerator(te, tMesh2);
        meshGenerator.generateMesh(0.25f, 0);

        publishProgress("Centering object...");
        ObjectCenter.centerObject(tMesh2);

        publishProgress("Setting up view nodes");
        HalfEdgeTriangleMeshNode hfn = new HalfEdgeTriangleMeshNode(tMesh2, triangulation.getTriangles(), sp, te, true);
        ContourTestNode ctn = new ContourTestNode(Pixel.getNormedDrawnPoints());
        Camera.setupLookAtMatrix(ObjectCenter.fDistance);
        TranslationNode trn = new TranslationNode(new Vector(-ObjectCenter.p.x(), -ObjectCenter.p.y(), -ObjectCenter.p.z()));
        trn.addChild(hfn);

        publishProgress("Finished");
        ViewerActivity.addToChildren(trn);
    //    ViewerActivity.addToMultipleChildren(ctn);

        return null;
    }

    public TriangleElevation getTriangleElevation() {
        return te;
    }

    protected void onProgressUpdate(String... message) {
        super.onProgressUpdate(message);
        Log.e("Message", message[0]);
        dialog.setMessage(message[0]);
    }
}
