package com.copmutergrafik.stefanwagner.a3d_viewerbuilder.rendering;

import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.math.MathHelpers.Vector;

/**
 * Created by Stefan Wagner on 30.01.2017.
 */

public class RenderVertex {
    public RenderVertex(Vector position, Vector normal, Vector color) {
        this.position = position;
        this.normal = normal;
        this.color = color;
    }

    /**
     * 3D position.
     */
    public Vector position;

    /**
     * 3D normal.
     */
    public Vector normal;

    /**
     * 4D color.
     */
    public Vector color;

}
