package com.copmutergrafik.stefanwagner.a3d_viewerbuilder.builder;

import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.math.MathHelpers.MathHelpers;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.math.MathHelpers.Vector;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.math.MathHelpers.VectorTriangle;

import java.util.ArrayList;

/**
 * Created by Stefan on 06.02.2017.
 * This class applies the ear-cutting triangulation to a set of contour points
 */

public class Triangulation {
    ArrayList<Vector> originalVertices;
    ArrayList<Vector> vertices;
    ArrayList<Integer> convexVertices;
    ArrayList<Integer> concaveVertices;
    ArrayList<Integer> ears;
    ArrayList<VectorTriangle> triangles;

    public Triangulation(ArrayList<Vector> vertices) {
        this.originalVertices = (ArrayList) vertices.clone();
        this.vertices = vertices;
        this.convexVertices = new ArrayList<>();
        this.concaveVertices = new ArrayList<>();
        this.ears = new ArrayList<>();
        this.triangles = new ArrayList<>();

        splitConvexConcave();
        findEars();
    }

    /**
     * Perform triangulation by adding triangles to the triangle list then removing the corresponding
     * vertices and finally reclassifying the remaining vertices
     */
    public void triangulate() {
        while (ears.size() > 0) {
            Integer index = ears.get(0);

            triangles.add(new VectorTriangle(vertices.get(MathHelpers.PositiveMod(index - 1, vertices.size())),
                    vertices.get(MathHelpers.PositiveMod(index, vertices.size())),
                    vertices.get(MathHelpers.PositiveMod(index + 1, vertices.size()))));

            ears.remove(0);
            convexVertices.remove(new Integer(index));
            concaveVertices.remove(new Integer(index));
            vertices.remove((int) index);

            splitConvexConcave();
            findEars();
        }
    }

    /**
     * Find ears in convex vertex list and check if any concave vertex intersects the possible triangle
     */
    public void findEars() {
        ears = new ArrayList<>();
        boolean isEar = true;
        for (int i = 0; i < convexVertices.size(); i++) {
            isEar = true;
            for (int j = 0; j < concaveVertices.size(); j++) {
                if (pointInTriangle(vertices.get(concaveVertices.get(j)),
                        vertices.get(MathHelpers.PositiveMod(convexVertices.get(i) - 1, vertices.size())),
                        vertices.get(MathHelpers.PositiveMod(convexVertices.get(i), vertices.size())),
                        vertices.get(MathHelpers.PositiveMod(convexVertices.get(i) + 1, vertices.size())))) {
                    isEar = false;
                }
            }
            if (isEar) {
                ears.add(convexVertices.get(i));
            }
        }
    }

    /**
     * Classify vertices as either convex or concave
     */
    public void splitConvexConcave() {
        concaveVertices = new ArrayList<>();
        convexVertices = new ArrayList<>();
        for (int i = 0; i < vertices.size(); i++) {
            if (isConvex(vertices.get(MathHelpers.PositiveMod(i - 1, vertices.size())),
                    vertices.get(MathHelpers.PositiveMod(i, vertices.size())),
                    vertices.get(MathHelpers.PositiveMod(i + 1, vertices.size())))) {
                convexVertices.add(i);
            } else {
                concaveVertices.add(i);
            }
        }
    }

    /**
     * Check if vertex is convex
     *
     * @param V1_n Neighbor vertex n-1
     * @param Vn   Vertex to be determined
     * @param Vn_1 Neighbor vertex n+1
     * @return True if vertex is convex
     */
    public boolean isConvex(Vector V1_n, Vector Vn, Vector Vn_1) {
        float l = (float) (((V1_n.x() - Vn.x()) * (Vn_1.y() - Vn.y()))
                - ((V1_n.y() - Vn.y()) * (Vn_1.x() - Vn.x())));

        if (l < 0) {
            return true;
        }
        return false;
    }

    /**
     * Check if vertex is inside possible triangle by calculating and checking barycentric coordinates
     *
     * @param point Point to be checked against
     * @param V1_n  Neighbor vertex n-1
     * @param Vn    Vertex to be determined
     * @param Vn_1  Neighbor vertex n+1
     * @return True if vertex is inside possible triangle
     */
    public boolean pointInTriangle(Vector point, Vector V1_n, Vector Vn, Vector Vn_1) {
        float alpha = (float) (((Vn.y() - Vn_1.y()) * (point.x() - Vn_1.x()) + (Vn_1.x() - Vn.x()) * (point.y() - Vn_1.y())) /
                ((Vn.y() - Vn_1.y()) * (V1_n.x() - Vn_1.x()) + (Vn_1.x() - Vn.x()) * (V1_n.y() - Vn_1.y())));

        float beta = (float) (((Vn_1.y() - V1_n.y()) * (point.x() - Vn_1.x()) + (V1_n.x() - Vn_1.x()) * (point.y() - Vn_1.y())) /
                ((Vn.y() - Vn_1.y()) * (V1_n.x() - Vn_1.x()) + (Vn_1.x() - Vn.x()) * (V1_n.y() - Vn_1.y())));

        float gamma = 1.0f - alpha - beta;

        return (alpha > 0) && (beta > 0) && (gamma > 0);
    }

    public ArrayList<VectorTriangle> getTriangles() {
        return triangles;
    }

    public ArrayList<Vector> getOriginalVertices() {
        return originalVertices;
    }
}