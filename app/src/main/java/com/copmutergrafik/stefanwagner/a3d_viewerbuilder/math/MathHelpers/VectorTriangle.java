package com.copmutergrafik.stefanwagner.a3d_viewerbuilder.math.MathHelpers;

/**
 * Created by Stefan on 07.02.2017.
 * Representation of a triangle
 */

public class VectorTriangle {

    String id = "0";
    private Vector[] vectors = {new Vector(3), new Vector(3), new Vector(3)};
    private Vector normal = new Vector(3);

    public VectorTriangle(Vector p1, Vector p2, Vector p3) {
        vectors[0] = p1;
        vectors[1] = p2;
        vectors[2] = p3;

        computeNormal();
    }

    private void computeNormal() {
        Vector w = vectors[1].subtract(vectors[0]);
        Vector v = vectors[2].subtract(vectors[0]);
        normal = v.cross(w).getNormalized();
    }

    public Vector[] getTriangle() {
        return vectors;
    }

    public int getId() {
        return Integer.parseInt(id);
    }

    public void setId(String id) {
        this.id = id;
    }

    public Vector getNormal() {
        return normal;
    }
}
