package com.copmutergrafik.stefanwagner.a3d_viewerbuilder.scenegraph;

import android.opengl.GLES20;

import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.math.MathHelpers.Vector;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.rendering.RenderVertex;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.rendering.VertexBufferObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Stefan Wagner on 30.01.2017.
 */

public class CubeNode extends LeafNode {

    /**
     * Cube side length
     */
    private double sideLength;

    /**
     * VBO.
     */
    private VertexBufferObject vbo = new VertexBufferObject();

    /**
     * Constructor.
     */
    public CubeNode(double sideLength) {
        this.sideLength = sideLength;
        createVbo();
    }

    private void createVbo() {
        List<RenderVertex> renderVertices = new ArrayList<RenderVertex>();

        Vector p0 = new Vector(-sideLength, -sideLength, -sideLength);
        Vector p1 = new Vector(sideLength, -sideLength, -sideLength);
        Vector p2 = new Vector(sideLength, sideLength, -sideLength);
        Vector p3 = new Vector(-sideLength, sideLength, -sideLength);
        Vector p4 = new Vector(-sideLength, -sideLength, sideLength);
        Vector p5 = new Vector(sideLength, -sideLength, sideLength);
        Vector p6 = new Vector(sideLength, sideLength, sideLength);
        Vector p7 = new Vector(-sideLength, sideLength, sideLength);
        Vector n0 = new Vector(0, 0, -1);
        Vector n1 = new Vector(1, 0, 0);
        Vector n2 = new Vector(0, 0, 1);
        Vector n3 = new Vector(-1, 0, 0);
        Vector n4 = new Vector(0, 1, 0);
        Vector n5 = new Vector(0, -1, 0);
        Vector color = new Vector(0.5, 0.25, 0.75, 1);

        AddSideVertices(renderVertices, p7, p4, p6,n2,color);
        AddSideVertices(renderVertices, p4, p5, p6, n2, color);
        AddSideVertices(renderVertices, p6, p5, p2, n1, color);
        AddSideVertices(renderVertices, p5, p1, p2, n1, color);
        AddSideVertices(renderVertices, p2, p1, p3, n0, color);
        AddSideVertices(renderVertices, p1, p0, p3, n0, color);
        AddSideVertices(renderVertices, p3, p0, p7, n3, color);
        AddSideVertices(renderVertices, p0, p4, p7, n3, color);
        AddSideVertices(renderVertices, p3, p7, p2, n4, color);
        AddSideVertices(renderVertices, p7, p6, p2, n4, color);
        AddSideVertices(renderVertices, p1, p5, p0, n5, color);
        AddSideVertices(renderVertices, p5, p4, p0, n5, color);

        vbo.Setup(renderVertices, GLES20.GL_TRIANGLES);
    }

    /**
     * Add 4 vertices to the array
     */
    private void AddSideVertices(List<RenderVertex> renderVertices, Vector p0,
                                 Vector p1, Vector p2, Vector normal, Vector color) {

        renderVertices.add(new RenderVertex(p2, normal, color));
        renderVertices.add(new RenderVertex(p1, normal, color));
        renderVertices.add(new RenderVertex(p0, normal, color));
    }

    @Override
    public void drawGL() {
        vbo.draw();
    }
}
