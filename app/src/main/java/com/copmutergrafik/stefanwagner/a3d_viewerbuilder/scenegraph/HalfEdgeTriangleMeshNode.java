package com.copmutergrafik.stefanwagner.a3d_viewerbuilder.scenegraph;

import android.opengl.GLES20;

import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.builder.Skeleton;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.builder.TriangleElevation;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.halfedge.HalfEdge;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.halfedge.HalfEdgeTriangle;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.halfedge.HalfEdgeVertex;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.math.MathHelpers.VectorTriangle;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.mesh.IHalfEdgeTriangleMesh;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.rendering.RenderVertex;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.rendering.VertexBufferObject;

import java.util.ArrayList;
import java.util.List;

import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.math.MathHelpers.Vector;

/**
 * Created by Stefan on 02.02.2017.
 */

public class HalfEdgeTriangleMeshNode extends LeafNode {
    private VertexBufferObject vbo = new VertexBufferObject();
    private VertexBufferObject vboLines = new VertexBufferObject();
    private VertexBufferObject vboPoints = new VertexBufferObject();
    private VertexBufferObject vboGreenLines = new VertexBufferObject();
    private VertexBufferObject vboArcLines = new VertexBufferObject();
    private VertexBufferObject vboArc = new VertexBufferObject();

    private Skeleton skeleton;
    private TriangleElevation te;
    private boolean vertexNormalsEnabled;

    public HalfEdgeTriangleMeshNode(IHalfEdgeTriangleMesh mesh, ArrayList<VectorTriangle> triangles, Skeleton sp, TriangleElevation te, boolean vertex) {
        this.vertexNormalsEnabled = vertex;
        this.skeleton = sp;
        this.te = te;
        createVbo(mesh, triangles);
    }

    private void createVbo(IHalfEdgeTriangleMesh mesh, ArrayList<VectorTriangle> triangles) {
        List<RenderVertex> renderVertices = new ArrayList<RenderVertex>();
        List<RenderVertex> renderVerticesLines = new ArrayList<RenderVertex>();
        List<RenderVertex> renderVerticesPoints = new ArrayList<>();
        List<RenderVertex> renderVerticesGreen = new ArrayList<>();
        List<RenderVertex> renderVerticesArc = new ArrayList<>();
        List<RenderVertex> renderVerticesArcLines = new ArrayList<>();

        Vector normal1 = null;
        Vector normal2 = null;
        Vector normal3 = null;

        for (int i = 0; i < mesh.getNumberOfTriangles(); i++) {
            HalfEdgeTriangle facet = mesh.getHalfEdgeTriangle(i);

            /* Get half edges */
            HalfEdge he1 = facet.getHalfEdge();
            HalfEdge he2 = he1.getNext();
            HalfEdge he3 = he2.getNext();

            /* Get vertices */
            HalfEdgeVertex hv1 = he1.getStartVertex();
            HalfEdgeVertex hv2 = he2.getStartVertex();
            HalfEdgeVertex hv3 = he3.getStartVertex();

            /* get normals */
            if (vertexNormalsEnabled) {
                normal1 = hv1.getNormal();
                normal2 = hv2.getNormal();
                normal3 = hv3.getNormal();
            } else if (!vertexNormalsEnabled && facet.getNormal() != null) {
                normal1 = facet.getNormal();
                normal2 = facet.getNormal();
                normal3 = facet.getNormal();
            }

            Vector color;
          /*  if (Integer.parseInt(facet.getId()) == 1) {
                color = new Vector(0.149, 0.729, 0.231, 1);
            } else if (Integer.parseInt(facet.getId()) == 2) {
                color = new Vector(0.905, 0.152, 0.152, 1);
            } else if (Integer.parseInt(facet.getId()) == 3) {
                color = new Vector(0.949, 0.666, 0.149, 1);
            } else if (isJunction(facet)) {
                color = new Vector(0.905, 0.152, 0.152, 1);
            } else if (isSleeve(facet)) {
                color = new Vector(0.905, 0.901, 0.152, 1);
            } else {

            }
            */
            if (Integer.parseInt(facet.getId()) == -2) {
                color = new Vector(1, 1, 1, 1);
            } else if (Integer.parseInt(facet.getId()) == -1) {
                color = new Vector(0.905, 0.152, 0.152, 1);
            } else {
                color = new Vector(0.149, 0.949, 0.945, 1);
            }

            /* add render vertices */

            renderVertices.add(new RenderVertex(hv1.getPosition(), normal1, color));
            renderVertices.add(new RenderVertex(hv2.getPosition(), normal2, color));
            renderVertices.add(new RenderVertex(hv3.getPosition(), normal3, color));

        }

        for (int i = 0; i < triangles.size(); i++) {
            Vector p1 = triangles.get(i).getTriangle()[0];
            Vector p2 = triangles.get(i).getTriangle()[1];
            Vector p3 = triangles.get(i).getTriangle()[2];

            Vector color = null;

            if (triangles.get(i).getId() == -450) {
                color = new Vector(1, 1, 1, 1);
            } else {
                color = new Vector(0.149, 0.949, 0.945, 1);
            }

            renderVerticesArc.add(new RenderVertex(p1, triangles.get(i).getNormal(), color));
            renderVerticesArc.add(new RenderVertex(p2, triangles.get(i).getNormal(), color));
            renderVerticesArc.add(new RenderVertex(p3, triangles.get(i).getNormal(), color));

            // Calculate center
            Vector s = p1.add(p2).add(p3).multiply(1.0 / 3.0);

            // Calculate 0.1 of normal and add to center
            Vector d = s.add(triangles.get(i).getNormal().multiply(0.05));


            setLine(renderVerticesLines, new Vector(0.0, 0.0, 1.0), s, d);

        }

        for (int i = 0; i < skeleton.getSkeletonVertices().size() - 1; i++) {
            // setLine(renderVerticesLines, new Vector(0.0, 0.0, 1.0), skeleton.getSkeletonVertices().get(i).getPosition(), skeleton.getSkeletonVertices().get(i + 1).getPosition());
        }

        for (int i = 0; i < skeleton.getSkeletonVertices().size(); i++) {
            renderVerticesPoints.add(new RenderVertex(skeleton.getSkeletonVertices().get(i).getPosition(), new Vector(0.0, 0.0, 1.0), new Vector(0.905, 0.901, 0.152, 1)));
        }


        for (int j = 0; j < te.getSegmentSectorList().size(); j++) {
            for (int k = 0; k < te.getSegmentSectorList().get(j).size(); k++) {
                for (int h = 0; h < (((ArrayList<Vector>) te.getSegmentSectorList().get(j).get(k)).size()); h++) {
                    Vector test = (((ArrayList<Vector>) te.getSegmentSectorList().get(j).get(k)).get(h));
                    Vector color = null;
                   // if(k ==  te.getSegmentSectorList().get(j).size() - 1){
                    //   color = new Vector(0.905, 0.152, 0.152, 1);
                   // }else{
                       color = new Vector(1, 1, 1, 1);
                   // }
                    renderVerticesArcLines.add(new RenderVertex((((ArrayList<Vector>) te.getSegmentSectorList().get(j).get(k)).get(h)), new Vector(0.0, 0.0, -1.0), color));
                }
            }

        }


       /* for(int j = 0; j < 20; j++) {
            renderVerticesArcLines.add(new RenderVertex(((ArrayList<Vector>)te.getArcSegments().get(0)).get(j), new Vector(0.0, 0.0, -1.0), new Vector(0.149, 0.949, 0.945, 1)));
        }
        */


        vbo.Setup(renderVertices, GLES20.GL_TRIANGLES);
        vboLines.Setup(renderVerticesLines, GLES20.GL_LINES);
        // vboPoints.Setup(renderVerticesPoints, GLES20.GL_POINTS);
        //  vboGreenLines.Setup(renderVerticesGreen, GLES20.GL_TRIANGLES);
        vboArc.Setup(renderVerticesArc, GLES20.GL_LINES);
        vboArcLines.Setup(renderVerticesArcLines, GLES20.GL_LINES);

    }

    private void setLine(List<RenderVertex> renderVerticesLines, Vector normal, Vector v0, Vector v1) {
        renderVerticesLines.add(new RenderVertex(v0, normal, new Vector(1, 1, 1, 1)));
        renderVerticesLines.add(new RenderVertex(v1, normal, new Vector(1, 1, 1, 1)));
    }

    @Override
    public void drawGL() {
        //vbo.draw();
        //vboLines.draw();
        //vboPoints.draw();
        //vboGreenLines.draw();
        //vboArc.draw();
        vboArcLines.draw();
    }


    private boolean isSleeve(HalfEdgeTriangle facet) {
        if ((facet.getHalfEdge().getOpposite() == null
                && facet.getHalfEdge().getNext().getOpposite() != null
                && facet.getHalfEdge().getNext().getNext().getOpposite() != null) ||
                (facet.getHalfEdge().getOpposite() != null
                        && facet.getHalfEdge().getNext().getOpposite() == null
                        && facet.getHalfEdge().getNext().getNext().getOpposite() != null) ||
                (facet.getHalfEdge().getOpposite() != null
                        && facet.getHalfEdge().getNext().getOpposite() != null
                        && facet.getHalfEdge().getNext().getNext().getOpposite() == null)) {
            return true;
        }
        return false;
    }

    private boolean isJunction(HalfEdgeTriangle facet) {
        if (facet.getHalfEdge().getOpposite() != null
                && facet.getHalfEdge().getNext().getOpposite() != null
                && facet.getHalfEdge().getNext().getNext().getOpposite() != null) {
            return true;
        }
        return false;
    }
}
