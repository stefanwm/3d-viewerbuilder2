package com.copmutergrafik.stefanwagner.a3d_viewerbuilder;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.scenegraph.CubeNode;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.scenegraph.INode;

public class CanvasActivity extends Activity {
    private CanvasView customCanvas;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_canvas);

        customCanvas = (CanvasView) findViewById(R.id.canvas);
    }

    public void create3DObject(View view){
        customCanvas.create3DObject();
    }

    @Override
    public void onBackPressed(){
        Intent resultIntent = new Intent();
        Bundle resultBundle = new Bundle();
        resultBundle.putSerializable("0", customCanvas.getTask().getTriangleElevation());
        resultIntent.putExtras(resultBundle);

        setResult(Activity.RESULT_OK, resultIntent);
        finish();

        super.onBackPressed();
    }

}
