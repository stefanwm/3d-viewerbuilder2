package com.copmutergrafik.stefanwagner.a3d_viewerbuilder.halfedge;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.math.MathHelpers.Vector;

import java.io.Serializable;

/**
 * Created by Stefan on 02.02.2017.
 */

public class HalfEdgeVertex implements Serializable {

    public String getId() {
        return id;
    }

    public void setId(String id) { this.id = id;}

    /**
     * 3D position of the vertex.
     */

    private String id = "0";

    private final Vector position = new Vector(0, 0, 0);

    public Vector getCentroid() {
        return centroid;
    }

    public void setCentroid(Vector centroid) {
        this.centroid = centroid;
    }

    private Vector centroid = new Vector(0, 0, 0);

    /**
     * (Normalized) normal direction of the vertex.
     */
    private Vector normal = new Vector(1, 0, 0);

    /**
     * Color value at the vertex
     */
    private Vector color = new Vector(0, 0, 0);

    /**
     * Reference to one of the outgoing half edges.
     */
    private HalfEdge halfEgde = null;

    /**
     * Constructor.
     *
     * @param position
     *          Initial value for position.
     */

    public HalfEdgeVertex(Vector position){
        this.position.copy(position);
    }

    public HalfEdgeVertex(Vector position, String id) {
        this.position.copy(position);
        this.id = id;
    }

    /**
     * Constructor.
     *
     * @param position
     *          Initial value for position.
     * @param normal
     *          Initial value for normal.
     */
    public HalfEdgeVertex(Vector position, Vector normal) {
        this.position.copy(position);
        this.normal.copy(normal);
    }

    /**
     * Constructor.
     *
     * @param position
     *          Initial value for position.
     * @param normal
     *          Initial value for normal.
     */
    public HalfEdgeVertex(Vector position, Vector normal, Vector color) {
        this.position.copy(position);
        this.normal.copy(normal);
        this.color.copy(color);
    }

    public Vector getPosition() {
        return position;
    }

    public Vector getNormal() {
        return normal;
    }

    public Vector getColor() {
        return color;
    }

    public void setNormal(Vector normal) {
        this.normal.copy(normal);
    }

    public void setColor(Vector color) {
        this.color.copy(color);
    }

    public HalfEdge getHalfEdge() {
        return halfEgde;
    }

    public void setHalfEgde(HalfEdge halfEgde) {
        this.halfEgde = halfEgde;
    }

    @Override
    public String toString() {
        return "Vertex";
    }
}
