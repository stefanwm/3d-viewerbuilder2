package com.copmutergrafik.stefanwagner.a3d_viewerbuilder.builder;

import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.math.MathHelpers.MathHelpers;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.math.MathHelpers.VectorTriangle;

import java.util.ArrayList;

import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.math.MathHelpers.Vector;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.mesh.HalfEdgeTriangleMesh;

/**
 * Created by Stefan on 22.02.2017.
 * This class generates an half edge mesh for an object by taking in elevated arc segments and "sewing" together.
 * Finally all arc segment sections are connected.
 */
public class MeshGenerator {
    private TriangleElevation te;
    private HalfEdgeTriangleMesh tMesh;
    private HalfEdgeTriangleMesh tMeshInverted;
    private ArrayList<VectorTriangle> triangles;
    private ArrayList<VectorTriangle> trianglesBefore;
    private ArrayList<VectorTriangle> trianglesBefore2;
    private ArrayList<VectorTriangle> trianglesBeforeOpposite;
    private ArrayList<ArrayList<Vector>> endSegments;

    int oldSegmentIndex = -1;
    int oldSegmentIndex2 = -1;

    public MeshGenerator(TriangleElevation te, HalfEdgeTriangleMesh tMesh) {
        this.te = te;
        this.tMesh = tMesh;

        this.tMeshInverted = new HalfEdgeTriangleMesh(true);
        triangles = new ArrayList<>();
        trianglesBefore = new ArrayList<>();

        trianglesBeforeOpposite = new ArrayList<>();
        this.endSegments = new ArrayList<>();
    }

    /**
     * Generate mesh by iterating through all arc segments and "sewing" these together
     *
     * @param size       Size of mesh. Value by which z-value of each arc segment vertex is reduced
     * @param iterations Number of iterations to be performed by laplace smoothing
     */
    public void generateMesh(float size, float iterations) {
        addVerticesToMesh(size);

        for (int segmentIndex = 0; segmentIndex < te.getSegmentSectorList().size(); segmentIndex++) {
            for (int arcIndex = 0; arcIndex < te.getSegmentSectorList().get(segmentIndex).size() - 1; arcIndex++) {
                ArrayList<Vector> firstArcSegment = null;
                ArrayList<Vector> secondArcSegment = null;
                ArrayList<Vector> firstArcSegmentOpposite = new ArrayList<>();
                ArrayList<Vector> secondArcSegmentOpposite = new ArrayList<>();

                //Get arc segment and neighboring arc segment
                firstArcSegment = (ArrayList<Vector>) ((ArrayList<Vector>) te.getSegmentSectorList().get(segmentIndex).get(arcIndex)).clone();
                secondArcSegment = (ArrayList<Vector>) ((ArrayList<Vector>) te.getSegmentSectorList().get(segmentIndex).get(arcIndex + 1)).clone();

                createOppositeArcSegments(firstArcSegment, secondArcSegment, firstArcSegmentOpposite, secondArcSegmentOpposite);

                //Create triangles with arc segments and arc segment opposites
                createTriangles(tMesh, firstArcSegment, secondArcSegment, segmentIndex);
                createTrianglesOpposite(tMesh, firstArcSegmentOpposite, secondArcSegmentOpposite, segmentIndex);
            }
        }

        //Get all end segments of each arc segment section
        for (int segmentIndex = 0; segmentIndex < te.getSegmentSectorList().size(); segmentIndex++) {
            endSegments.add((ArrayList<Vector>) ((ArrayList<Vector>) te.getSegmentSectorList().get(segmentIndex).get(te.getSegmentSectorList().get(segmentIndex).size() - 1)).clone());
        }

        //Iterate over end segments of every arc segment section
        for (int i = 0; i < endSegments.size(); i++) {
            ArrayList<Vector> firstArcSegment = null;
            ArrayList<Vector> secondArcSegment = null;
            ArrayList<Vector> firstArcSegmentOpposite = new ArrayList<>();
            ArrayList<Vector> secondArcSegmentOpposite = new ArrayList<>();

            firstArcSegment = (ArrayList<Vector>) endSegments.get(i).clone();
            secondArcSegment = (ArrayList<Vector>) te.getArcSegments().get((MathHelpers.PositiveMod(te.getArcSegments().indexOf(endSegments.get(i)) + 2, te.getArcSegments().size()))).clone();

            if (!firstArcSegment.equals(te.getArcSegments().get(te.getArcSegments().size() - 1))) {
                createOppositeArcSegments(firstArcSegment, secondArcSegment, firstArcSegmentOpposite, secondArcSegmentOpposite);

                createTriangles(tMeshInverted, firstArcSegment, secondArcSegment, oldSegmentIndex);
                createTrianglesOpposite(tMeshInverted, firstArcSegmentOpposite, secondArcSegmentOpposite, oldSegmentIndex2);
            }
        }

        tMeshInverted.computeTriangleNormals();

        tMesh.getFacetes().addAll(tMeshInverted.getFacetes());
        tMesh.computeTriangleNormalsAndJoinHalves();
    }

    /**
     * Create and add back side triangles.
     *
     * @param firstArcSegment  An arc segment
     * @param secondArcSegment Neighboring arc segment
     * @param segmentIndex     Current segment index
     */
    private void createTrianglesOpposite(HalfEdgeTriangleMesh mesh, ArrayList<Vector> firstArcSegment, ArrayList<Vector> secondArcSegment, int segmentIndex) {
        VectorTriangle t2Old = null;
        Vector triangleVertex3 = null;

        while (firstArcSegment.size() > 0 && secondArcSegment.size() > 0) {
            //Find triangle vertices by sewing vertices together
            Vector triangleVertex1 = firstArcSegment.get(0);
            firstArcSegment.remove(triangleVertex1);

            Vector triangleVertex2 = secondArcSegment.get(0);
            secondArcSegment.remove(triangleVertex2);

            if (firstArcSegment.size() > 0 && secondArcSegment.size() > 0) {
                triangleVertex3 = firstArcSegment.get(0);
            }

            //Add opposite triangle
            if (trianglesBeforeOpposite.size() > 0) {
                if (segmentIndex == oldSegmentIndex2 && firstArcSegment.size() < 19) {
                    t2Old = trianglesBeforeOpposite.get(trianglesBeforeOpposite.size() - 1);
                    mesh.addTriangle(t2Old.getTriangle()[2].getId(), t2Old.getTriangle()[1].getId(), triangleVertex2.getId());
                }
            }
            oldSegmentIndex2 = segmentIndex;

            //Add triangle
            if (firstArcSegment.size() > 0 && secondArcSegment.size() > 0) {
                mesh.addTriangle(triangleVertex1.getId(), triangleVertex2.getId(), triangleVertex3.getId());
                //Add to list to add opposite in next iteration
                trianglesBeforeOpposite.add(new VectorTriangle(triangleVertex1, triangleVertex2, triangleVertex3));
            }
        }
    }

    /**
     * Create and add front side triangles.
     *
     * @param firstArcSegment  An arc segment
     * @param secondArcSegment Neighboring arc segment
     * @param segmentIndex     Current segment index
     */
    private void createTriangles(HalfEdgeTriangleMesh mesh, ArrayList<Vector> firstArcSegment, ArrayList<Vector> secondArcSegment, int segmentIndex) {
        VectorTriangle t2Old = null;
        Vector triangleVertex3 = null;

        while (firstArcSegment.size() > 0 && secondArcSegment.size() > 0) {
            Vector triangleVertex1 = firstArcSegment.get(0);
            firstArcSegment.remove(triangleVertex1);

            Vector triangleVertex2 = secondArcSegment.get(0);
            secondArcSegment.remove(triangleVertex2);

            if (firstArcSegment.size() > 0 && secondArcSegment.size() > 0) {
                triangleVertex3 = firstArcSegment.get(0);
            }

            if (trianglesBefore.size() > 0) {
                if (segmentIndex == oldSegmentIndex && firstArcSegment.size() < 19) {
                    t2Old = trianglesBefore.get(trianglesBefore.size() - 1);

                    mesh.addTriangle(triangleVertex2.getId(), t2Old.getTriangle()[1].getId(), t2Old.getTriangle()[0].getId());
                }
            }
            oldSegmentIndex = segmentIndex;

            if (firstArcSegment.size() > 0 && secondArcSegment.size() > 0) {
                mesh.addTriangle(triangleVertex3.getId(), triangleVertex2.getId(), triangleVertex1.getId());
                trianglesBefore.add(new VectorTriangle(triangleVertex3, triangleVertex2, triangleVertex1));
            }
        }
    }

    /**
     * Add front vertices and back facing vertices to half edge mesh
     *
     * @param size Size by which the z-value of the vertices should be reduced
     */
    private void addVerticesToMesh(float size) {
        for (int i = 0; i < te.getArcVertices().size(); i++) {
            Vector vertex = new Vector(3);
            vertex.copy(te.getArcVertices().get(i));
            vertex.set(2, vertex.get(2) * size);
            vertex.setId(String.valueOf(i));
            tMesh.addVertex(vertex, vertex.getId());
        }

        for (int i = te.getArcVertices().size(); i < te.getArcVertices().size() + te.getArcVertices().size(); i++) {
            Vector oppositeVertex = new Vector(3);
            oppositeVertex.copy(te.getArcVertices().get(i - te.getArcVertices().size()));
            oppositeVertex.set(2, oppositeVertex.get(2) * -size);
            oppositeVertex.setId(String.valueOf(i));

            tMesh.addVertex(oppositeVertex, oppositeVertex.getId());
        }

        tMeshInverted.getHalfEdgeVertices().addAll(tMesh.getHalfEdgeVertices());
    }

    /**
     * Fill back facing arc segment lists from front facing arc segment lists
     *
     * @param firstArcSegment          An arc segment list
     * @param secondArcSegment         Neighboring arc segment list
     * @param firstArcSegmentOpposite  Back-facing arc segment list
     * @param secondArcSegmentOpposite Neighboring back-facing arc segment list
     */
    private void createOppositeArcSegments(ArrayList<Vector> firstArcSegment, ArrayList<Vector> secondArcSegment, ArrayList<Vector> firstArcSegmentOpposite, ArrayList<Vector> secondArcSegmentOpposite) {
        //Get arc segment opposite and neighboring arc segment opposite
        for (int i = 0; i < firstArcSegment.size(); i++) {
            Vector oppositeVector = new Vector(3);
            oppositeVector.copy(firstArcSegment.get(i));
            oppositeVector.setId(String.valueOf(firstArcSegment.get(i).getId()));
            firstArcSegmentOpposite.add(oppositeVector);

            if (secondArcSegment != null) {
                Vector oppositeVector2 = new Vector(3);
                oppositeVector2.copy(secondArcSegment.get(i));
                oppositeVector2.setId(String.valueOf(secondArcSegment.get(i).getId()));
                secondArcSegmentOpposite.add(oppositeVector2);
            }
        }

        //Set new id's of opposite arc segments
        for (int i = 0; i < firstArcSegmentOpposite.size(); i++) {
            firstArcSegmentOpposite.get(i).setId(String.valueOf(firstArcSegmentOpposite.get(i).getId() + te.getArcVertices().size()));
            secondArcSegmentOpposite.get(i).setId(String.valueOf(secondArcSegmentOpposite.get(i).getId() + te.getArcVertices().size()));
        }
    }

    public ArrayList<VectorTriangle> getTriangles() {
        return triangles;
    }
}