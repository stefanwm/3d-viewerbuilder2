package com.copmutergrafik.stefanwagner.a3d_viewerbuilder.scenegraph;

import android.opengl.GLES20;

import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.math.MathHelpers.VectorTriangle;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.rendering.RenderVertex;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.rendering.VertexBufferObject;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.math.MathHelpers.Vector;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Stefan on 07.02.2017.
 */

public class TriangleRootNode extends LeafNode {
    private VertexBufferObject vbo = new VertexBufferObject();

    public TriangleRootNode(ArrayList<VectorTriangle> triangle) {
        createVbo(triangle);
    }

    private void createVbo(ArrayList<VectorTriangle> cubes) {

        List<RenderVertex> renderVertices = new ArrayList<RenderVertex>();

        for (int i = 0; i < cubes.size(); i++) {
            VectorTriangle triangle = cubes.get(i);

            Vector p0 = triangle.getTriangle()[0];
            Vector p1 = triangle.getTriangle()[1];
            Vector p2 = triangle.getTriangle()[2];

            Vector normal = triangle.getNormal();
            Vector color = new Vector(0.5, 0.25, 0.75, 1);

            AddSideVertices(renderVertices, p0, p1, p2, normal, color);
        }
        vbo.Setup(renderVertices, GLES20.GL_TRIANGLES);
    }

    private void AddSideVertices(List<RenderVertex> renderVertices, Vector p0, Vector p1, Vector p2, Vector normal,
                                 Vector color) {
        renderVertices.add(new RenderVertex(p0, normal, new Vector(0.5, 0.25, 0.75, 1)));
        renderVertices.add(new RenderVertex(p1, normal,new Vector(0.5, 0.25, 0.75, 1)));
        renderVertices.add(new RenderVertex(p2, normal, new Vector(0.5, 0.25, 0.75, 1)));
    }

    @Override
    public void drawGL() {
        vbo.draw();
    }
}
