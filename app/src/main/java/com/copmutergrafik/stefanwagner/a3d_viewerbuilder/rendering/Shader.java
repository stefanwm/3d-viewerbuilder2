package com.copmutergrafik.stefanwagner.a3d_viewerbuilder.rendering;

import android.opengl.GLES20;
import android.util.Log;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.util.HashMap;
import java.util.Map;

/**
 * Representation of GLSL shader.
 *
 * @author Philipp Jenke
 */

public class Shader {

    /**
     * Shader constants.
     */
    private static final int COMPILE_STATUS_OK = 1;
    private static String context;

    public static enum ShaderType {
        VERTEX, FRAGMENT
    }

    ;

    public static enum ShaderMode {
        PHONG, TEXTURE, NO_LIGHTING, GOURAUD, AMBIENT_ONLY
    }

    /**
     * Flag for the state of the shaders
     */
    private boolean compiled = false;

    private static boolean shaderModified = false;
    /**
     * ID of the shader program.
     */
    private int shaderProgram = -1;

    /**
     * Filename of the vertex shader source
     */
    private String vertexShaderFilename = "";

    /**
     * Filename of the pixel shader source
     */
    private String fragmentShaderFilename = "";

    /**
     * Current shader mode
     */
    private static ShaderMode mode = ShaderMode.PHONG;

    public Shader(ShaderMode mode, String context) {
        this.mode = mode;
        this.context = context;

        if (mode == ShaderMode.PHONG) {
            this.vertexShaderFilename = "vertex_shader.glsl";
            this.fragmentShaderFilename = "fragment_shader.glsl";
        } else if (mode == ShaderMode.GOURAUD) {
            this.vertexShaderFilename = "vertex_shader_gr.glsl";
            this.fragmentShaderFilename = "fragment_shader_gr.glsl";
        }
    }

    public Shader(String vertexShaderFilename, String fragmentShaderFilename,
                  ShaderMode mode, String context) {
        this.mode = mode;
        this.vertexShaderFilename = vertexShaderFilename;
        this.fragmentShaderFilename = fragmentShaderFilename;
        this.context = context;
    }

    /**
     * Set different shader mode at runtime
     * @param mode Gouraud or Phong
     */
    public void setShader(ShaderMode mode) {
        if (mode == ShaderMode.PHONG) {
            this.vertexShaderFilename = "vertex_shader.glsl";
            this.fragmentShaderFilename = "fragment_shader.glsl";
        } else if (mode == ShaderMode.GOURAUD) {
            this.vertexShaderFilename = "vertex_shader_gr.glsl";
            this.fragmentShaderFilename = "fragment_shader_gr.glsl";
        }
        this.mode = mode;
    }

    /**
     * Compile and link the shaders.
     */
    public int compileAndLink(final String[] attributes) {
        compiled = true;

        checkGlError();

        // Compile
        int v = compileShader(getGlShaderType(ShaderType.VERTEX),
                vertexShaderFilename);
        int f = compileShader(getGlShaderType(ShaderType.FRAGMENT),
                fragmentShaderFilename);
        if (v < 0 || f < 0) {
            System.out.println("Shader not created.");
            return -1;
        }

        checkGlError();

        // Link
        shaderProgram = linkProgram(v, f, attributes);
        if (shaderProgram < 0) {
            System.out.println("Shader not created.");
            return -1;
        }

        System.out
                .println("Successfully created shader from vertex shader filename "
                        + vertexShaderFilename + " and fragment shader fileame "
                        + fragmentShaderFilename);
        return shaderProgram;
    }

    /**
     * Activate the shader
     */
    public void use(int program) {
        /*if (!isCompiled()) {
            compileAndLink();
            checkGlError();
        }*/
        checkGlError();
        GLES20.glUseProgram(program);
    }

    /**
     * Getter.
     */
    public boolean isCompiled() {
        return compiled;
    }

    /**
     * Read a shader code from a source file to a String.
     */
    private static String readShaderSource(String shaderFilename) {
        String absoluteShaderFilename = shaderFilename;
        String shaderSource = "";
        if (absoluteShaderFilename == null) {
            System.out.println("Shader source " + shaderFilename
                    + " not found - cannot read shader.");
            return shaderSource;
        }

        BufferedReader br = null;
        try {
            Log.d("INFO", context + "/" + absoluteShaderFilename);
            br = new BufferedReader(new FileReader(context + "/" + absoluteShaderFilename));
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();
            while (line != null) {
                sb.append(line);
                sb.append("\n");
                line = br.readLine();
            }
            br.close();
            shaderSource = sb.toString();
        } catch (FileNotFoundException e) {
            System.out
                    .println("Failed to read shader source " + absoluteShaderFilename);
            e.printStackTrace();
        } catch (IOException e) {
            System.out
                    .println("Failed to read shader source " + absoluteShaderFilename);
            e.printStackTrace();
        }
        return shaderSource;
    }

    /**
     * Convert to GL shader constants.
     */
    private int getGlShaderType(Shader.ShaderType type) {
        if (type == Shader.ShaderType.VERTEX) {
            return GLES20.GL_VERTEX_SHADER;
        } else if (type == Shader.ShaderType.FRAGMENT) {
            return GLES20.GL_FRAGMENT_SHADER;
        } else {
            return -1;
        }
    }

    /**
     * Link the vertex and fragment shaders.
     */
    private int linkProgram(int vertexShaderId, int fragmentShaderId, final String[] attributes) {
        checkGlError();
        int shaderProgram = GLES20.glCreateProgram();
        GLES20.glAttachShader(shaderProgram, vertexShaderId);
        GLES20.glAttachShader(shaderProgram, fragmentShaderId);

        // Bind attributes
        if (attributes != null) {
            final int size = attributes.length;
            for (int i = 0; i < size; i++) {
                GLES20.glBindAttribLocation(shaderProgram, i, attributes[i]);
            }
        }

        GLES20.glLinkProgram(shaderProgram);
        GLES20.glValidateProgram(shaderProgram);
        checkGlError();
        return shaderProgram;
    }

    /**
     * Compile the specified shader from the filename and return the OpenGL id.
     */
    private int compileShader(int shaderType, String shaderFilename) {
        String vsrc = Shader.readShaderSource(shaderFilename);
        int id = compileShaderFromSource(shaderType, vsrc);
        if (id < 0) {
            System.out.println("Compile error in shader file " + shaderFilename);
        }
        return id;
    }

    /**
     * Compile the specified shader from the filename and return the OpenGL id.
     */
    private int compileShaderFromSource(int shaderType,
                                        String shaderSource) {
        int id = GLES20.glCreateShader(shaderType);
        GLES20.glShaderSource(id, shaderSource);
        GLES20.glCompileShader(id);
        if (checkCompileError(id)) {
            String errorMsg = getCompileErrorMessage(id);
            System.out.println(errorMsg);
            return -1;
        }
        return id;
    }

    /**
     * Extract the error message.
     */
    private String getCompileErrorMessage(int id) {
        IntBuffer intBuffer = IntBuffer.allocate(1);
        GLES20.glGetShaderiv(id, GLES20.GL_INFO_LOG_LENGTH, intBuffer);
        int size = intBuffer.get(0);
        String errorMessage = "";
        if (size > 0) {
            ByteBuffer byteBuffer = ByteBuffer.allocate(size);
            Log.e("INFO", GLES20.glGetShaderInfoLog(id));
            for (byte b : byteBuffer.array()) {
                errorMessage += (char) b;
            }
        }
        return errorMessage;
    }

    /**
     * Check if a compile error (vertex or fragment shader) occurred?
     */
    private boolean checkCompileError(int id) {
        IntBuffer intBuffer = IntBuffer.allocate(1);
        GLES20.glGetShaderiv(id, GLES20.GL_COMPILE_STATUS, intBuffer);
        return intBuffer.get(0) != COMPILE_STATUS_OK;
    }

    public static void checkGlError() {
        Map<Integer, String> glErrorMap = new HashMap<Integer, String>();
        glErrorMap.put(GLES20.GL_NO_ERROR, "GL_NO_ERROR");
        glErrorMap.put(GLES20.GL_INVALID_ENUM, "GL_INVALID_ENUM");
        glErrorMap.put(GLES20.GL_INVALID_VALUE, "GL_INVALID_VALUE");
        glErrorMap.put(GLES20.GL_INVALID_OPERATION, "GL_INVALID_OPERATION");
        glErrorMap.put(GLES20.GL_OUT_OF_MEMORY, "GL_OUT_OF_MEMORY");
        glErrorMap.put(GLES20.GL_INVALID_FRAMEBUFFER_OPERATION,
                "GL_INVALID_FRAMEBUFFER_OPERATION");
        int err = GLES20.GL_NO_ERROR;
        do {
            err = GLES20.glGetError();
            if (err != GLES20.GL_NO_ERROR) {
                if (glErrorMap.containsKey(err)) {
                    System.out.println("GL error: " + glErrorMap.get(err));
                } else {
                    // System.out.println("Undefined GL error: " + glErrorMap.get(err));
                }
            }
        } while (err != GLES20.GL_NO_ERROR);
    }

    public static void setMode(ShaderMode mode) {
        mode = mode;
    }

    public static ShaderMode getMode() {
        return mode;
    }

    public int getProgram() {
        return shaderProgram;
    }

    public static boolean isShaderModified() {
        return shaderModified;
    }

    public static void setShaderModified(boolean isShaderModified) {
        shaderModified = isShaderModified;
    }

}
