package com.copmutergrafik.stefanwagner.a3d_viewerbuilder.builder;

import android.util.Log;

import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.halfedge.HalfEdge;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.halfedge.HalfEdgeTriangle;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.halfedge.HalfEdgeVertex;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.mesh.IHalfEdgeTriangleMesh;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.math.MathHelpers.Vector;


import java.util.ArrayList;

/**
 * Created by Stefan on 09.02.2017.
 */

public class SimplifyMesh {
    private IHalfEdgeTriangleMesh tMesh;
    private ArrayList<HalfEdgeTriangle> t_Triangles;



    private ArrayList<HalfEdge> innerHalfEdges;
    private ArrayList<HalfEdge> halfEdgesToBeRemoved;
    private ArrayList<HalfEdgeTriangle> halfEdgeTrianglesToBeRemoved;

    private boolean finish = false;

    public SimplifyMesh(IHalfEdgeTriangleMesh tMesh) {
        this.tMesh = tMesh;
        this.t_Triangles = new ArrayList<>();
        this.innerHalfEdges = new ArrayList<>();
        this.halfEdgesToBeRemoved = new ArrayList<>();
        this.halfEdgeTrianglesToBeRemoved = new ArrayList<>();

        //find_T_Triangles();
        Log.e("Triangles", String.valueOf(t_Triangles.size()));
        //simplifyTriangles();
    }

    public void simplifyTriangles() {
        for (int i = 0; i < t_Triangles.size(); i++) {

            //Get facet of current T-triangle
            HalfEdge HE = searchForOpposite(t_Triangles.get(i));

            boolean semicircle = checkInsideSemicircle(HE);
            boolean isJunction = isJunction(HE.getOpposite().getFacet());

            int k = 0;
            while (semicircle && !isJunction && !finish && HE.getOpposite() != null) {
                //He has opposite so get opposite to get other facet
                HalfEdge HE2 = HE.getOpposite();

                Log.e("Null", "searchBeforeOpposite");
                HalfEdge halfEdgeFacet1 = searchHEBeforeOpposite(HE);
                Log.e("Done", "searchBeforeOpposite");

                HalfEdge halfEdgeFacet2ToBeSetNext = HE2.getNext();
                HalfEdge halfEdgeFacet2 = HE2.getNext().getNext();
                HalfEdge halfEdgeFacet1ToBeSetNext = HE.getNext();

                halfEdgeFacet1.setNext(halfEdgeFacet2ToBeSetNext);
                halfEdgeFacet2.setNext(halfEdgeFacet1ToBeSetNext);

                halfEdgesToBeRemoved.add(HE);
                halfEdgesToBeRemoved.add(HE2);
                halfEdgeTrianglesToBeRemoved.add(HE.getFacet());
                halfEdgeTrianglesToBeRemoved.add(HE2.getFacet());

                Log.e("Null", "SearchForNextOpposite");
                HE = searchForNextOpposite(HE2.getOpposite());
                Log.e("Done", "SearchForNextOpposite");

                semicircle = checkInsideSemicircle(HE);
                if(HE.getOpposite() == null){
                    break;
                }
                isJunction = isJunction(HE.getOpposite().getFacet());

                Log.e("semicircle", String.valueOf(semicircle));
                //    Log.e("jsemicircle2", String.valueOf(semicircle2));
                Log.e("isJunction", String.valueOf(isJunction));
                k++;
            }

            if(k != 0){
                halfEdgeTrianglesToBeRemoved.remove(HE.getFacet());
                halfEdgeTrianglesToBeRemoved.remove(HE.getOpposite().getFacet());
            }
            if(HE.getOpposite() != null) {
                finish = false;
                innerHalfEdges.add(HE);

            }
            tMesh.getHalfEdges().removeAll(halfEdgesToBeRemoved);
            tMesh.getFacetes().removeAll(halfEdgeTrianglesToBeRemoved);
            fanTriangle(HE);
        }
    }


    public void fanTriangle(HalfEdge innerHalfEdge) {
        Vector a = innerHalfEdge.getStartVertex().getPosition();
        Vector b = innerHalfEdge.getEndVertex().getPosition();
        Vector centerPoint = a.add(b).multiply(0.5);

        HalfEdgeVertex centerHalfEdgeVertex = new HalfEdgeVertex(centerPoint);
        tMesh.getHalfEdgeVertices().add(centerHalfEdgeVertex);

        createHalfEdgeTriangle(innerHalfEdge, centerHalfEdgeVertex);
    }

    private void createHalfEdgeTriangle(HalfEdge innerHalfEdge, HalfEdgeVertex centerVertex) {
        HalfEdge adjustedInnerEdge = new HalfEdge();
        adjustedInnerEdge.setNext(innerHalfEdge.getNext());
        adjustedInnerEdge.setStartVertex(centerVertex);
        adjustedInnerEdge.setEndVertex(innerHalfEdge.getEndVertex());
        adjustedInnerEdge.setOpposite(innerHalfEdge.getOpposite());

        HalfEdge currentOuterEdge = innerHalfEdge.getNext();
        //Get last opposite to set next for opposite
        HalfEdge lastOpposite = adjustedInnerEdge;
        //Set next of last opposite
        innerHalfEdge.setNext(currentOuterEdge);

        int i = 0;
        while (currentOuterEdge.getOpposite() == null) {
            //Create facet and interior opposite
            HalfEdge interiorOppositeHE = new HalfEdge();
            HalfEdgeTriangle facet = new HalfEdgeTriangle();

            //Setup interior opposite
            interiorOppositeHE.setStartVertex(currentOuterEdge.getEndVertex());
            interiorOppositeHE.setEndVertex(centerVertex);
            interiorOppositeHE.setFacet(facet);
            interiorOppositeHE.setNext(lastOpposite);

            //Setup outer opposite
            HalfEdge outerOppositeHE = new HalfEdge();
            outerOppositeHE.setStartVertex(centerVertex);
            outerOppositeHE.setEndVertex(currentOuterEdge.getEndVertex());
            outerOppositeHE.setNext(currentOuterEdge.getNext());

            //Set opposites
            outerOppositeHE.setOpposite(interiorOppositeHE);
            interiorOppositeHE.setOpposite(outerOppositeHE);

            //Set facet to interior HalfEdge
            facet.setHalfEdge(interiorOppositeHE);

            //DEBUG
            if(currentOuterEdge.getNext().getNext().getOpposite() != null){
                facet.setId(String.valueOf(2));
            }else if(i == 0){
                facet.setId(String.valueOf(3));
            }else{
                facet.setId(String.valueOf(1));
            }

            //Store facet and half Edges
            tMesh.getFacetes().add(facet);
            tMesh.getHalfEdges().add(interiorOppositeHE);
            tMesh.getHalfEdges().add(outerOppositeHE);

            //Current outer edge has to be set to interior opposite to complete the triangle
            currentOuterEdge.setNext(interiorOppositeHE);

            //Get next outer edge
            currentOuterEdge = currentOuterEdge.getNext().getOpposite().getNext();

            //Get next last opposite
            lastOpposite = lastOpposite.getNext().getNext().getOpposite();
            //DEBUG
            i++;
        }
    }

    public HalfEdge searchForNextOpposite(HalfEdge halfEdge) {
        int i = 0;
        HalfEdge firstOpposite = halfEdge.getNext();
        while (firstOpposite.getOpposite() == null) {
            Log.e("SFNIteration", String.valueOf(i));
            firstOpposite = firstOpposite.getNext();
        }
        return firstOpposite;
    }

    private boolean checkInsideSemicircle(HalfEdge HE) {
        boolean insideSemicircle = true;
        HalfEdge HEBeforeOpposite = HE.getNext();

        int i = 0;
        while (HEBeforeOpposite.getOpposite() == null) {
            i++;
            if (!isInsideSemicircle(HEBeforeOpposite.getStartVertex().getPosition(), HE)) {
                insideSemicircle = false;
                break;
            }
            HEBeforeOpposite = HEBeforeOpposite.getNext();
        }
        insideSemicircle &= isInsideSemicircle(HEBeforeOpposite.getStartVertex().getPosition(), HE);
        Log.e("SMIterations", String.valueOf(i));
        return insideSemicircle;
    }


    public HalfEdge searchHEBeforeOpposite(HalfEdge halfEdge) {
        HalfEdge HEBeforeOpposite = halfEdge;
        while (HEBeforeOpposite.getNext().getOpposite() == null) {
            HEBeforeOpposite = HEBeforeOpposite.getNext();
        }
        return HEBeforeOpposite;
    }

    public HalfEdge searchForOpposite(HalfEdgeTriangle facet) {
        HalfEdge HEWithOpposite = facet.getHalfEdge();
        while (HEWithOpposite.getOpposite() == null) {
            HEWithOpposite = HEWithOpposite.getNext();
        }
        return HEWithOpposite;
    }

    private void find_T_Triangles() {
        for (int i = 0; i < tMesh.getNumberOfTriangles(); i++) {
            HalfEdgeTriangle facet = tMesh.getHalfEdgeTriangle(i);
            if (facet.getHalfEdge().getOpposite() == null && facet.getHalfEdge().getNext().getOpposite() == null && facet.getHalfEdge().getNext().getNext().getOpposite() != null
                    || facet.getHalfEdge().getOpposite() != null && facet.getHalfEdge().getNext().getOpposite() == null && facet.getHalfEdge().getNext().getNext().getOpposite() == null
                    || facet.getHalfEdge().getOpposite() == null && facet.getHalfEdge().getNext().getOpposite() != null && facet.getHalfEdge().getNext().getNext().getOpposite() == null) {
                t_Triangles.add(facet);
            }
        }
    }

    private boolean isSleeve(HalfEdgeTriangle facet) {
        if (facet.getHalfEdge().getOpposite() == null
                || facet.getHalfEdge().getNext().getOpposite() == null
                || facet.getHalfEdge().getNext().getNext().getOpposite() == null) {
            return true;
        }
        return false;
    }

    private boolean isJunction(HalfEdgeTriangle facet) {
        if (facet.getHalfEdge().getOpposite() != null
                && facet.getHalfEdge().getNext().getOpposite() != null
                && facet.getHalfEdge().getNext().getNext().getOpposite() != null) {
            return true;
        }
        return false;
    }

    public ArrayList<HalfEdge> getInnerHalfEdges() {
        return innerHalfEdges;
    }

    public boolean isInsideSemicircle(Vector point, HalfEdge diameter) {
        boolean result = false;
        Vector a = diameter.getStartVertex().getPosition();
        Vector b = diameter.getEndVertex().getPosition();

        float radius = distance(a, b) / 2;
        Vector centerPoint = a.add(b).multiply(0.5);

        Vector vector1 = b.subtract(centerPoint);
        Vector vector2 = point.subtract(centerPoint);
        Vector crossProduct = vector1.cross(vector2);

        float distance = distance(centerPoint, point);
        boolean preResult = distance <= radius;
        result = crossProduct.z() >= 0 && distance <= radius;
        return result;
    }

    private float distance(Vector p1, Vector p2) {
        return (float) Math.sqrt(Math.pow(p1.x() - p2.x(), 2) + Math.pow(p1.y() - p2.y(), 2) + Math.pow(p1.z() - p2.z(), 2));
    }
}
