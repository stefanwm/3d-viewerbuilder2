package com.copmutergrafik.stefanwagner.a3d_viewerbuilder.mesh;

import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.halfedge.HalfEdge;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.halfedge.HalfEdgeTriangle;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.halfedge.HalfEdgeVertex;

import java.util.ArrayList;

/**
 * Created by Stefan on 02.02.2017.
 */

public interface IHalfEdgeTriangleMesh extends ITriangleMesh {

    public HalfEdgeVertex getHalfEdgeVertex(int index);

    public HalfEdgeTriangle getHalfEdgeTriangle(int index);

    public ArrayList<HalfEdge> getHalfEdges();

    public ArrayList<HalfEdgeTriangle> getFacetes();

    public ArrayList<HalfEdgeVertex> getHalfEdgeVertices();

    public String getId();
}
