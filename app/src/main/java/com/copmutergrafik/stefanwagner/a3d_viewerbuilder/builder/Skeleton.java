package com.copmutergrafik.stefanwagner.a3d_viewerbuilder.builder;

import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.halfedge.HalfEdge;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.halfedge.HalfEdgeTriangle;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.halfedge.HalfEdgeVertex;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.math.MathHelpers.MathHelpers;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.math.MathHelpers.Vector;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.mesh.HalfEdgeTriangleMesh;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Stefan on 12.02.2017.
 * This class creates the skeleton of the triangulation and elevates the spine vertices
 * to create an elevated spine
 */

public class Skeleton implements Serializable {
    private ArrayList<HalfEdgeVertex> skeletonVertices;

    public Skeleton(ArrayList<HalfEdge> fannedTrianglesInnerEdges, HalfEdgeTriangleMesh tMesh) {
        this.skeletonVertices = new ArrayList<>();

        for (HalfEdgeTriangle facet : tMesh.getFacetes()) {
            if (Integer.parseInt(facet.getId()) != 1 && Integer.parseInt(facet.getId()) != 2 && Integer.parseInt(facet.getId()) != 3) {
                createSkeletonVertices(facet);
            }
        }
    }

    /**
     * Elevate skeleton vertices by getting average between center point and both start and end points
     */
    public void elevateSpine() {
        for (int i = 0; i < skeletonVertices.size(); i++) {
            HalfEdge spineHE = skeletonVertices.get(i).getHalfEdge();

            float distance1 = MathHelpers.getDistanceBetweenPoints(skeletonVertices.get(i).getPosition(), spineHE.getEndVertex().getPosition());
            float distance2 = MathHelpers.getDistanceBetweenPoints(skeletonVertices.get(i).getPosition(), spineHE.getStartVertex().getPosition());

            skeletonVertices.get(i).getPosition().set(2, (distance1 + distance2) / 2);
        }
    }

    /**
     * Get opposite edge of edge triangle facet and add as spine vertex
     *
     * @param facet Triangle facet
     */
    private void createSkeletonVertices(HalfEdgeTriangle facet) {
        HalfEdge start = facet.getHalfEdge();

        HalfEdgeVertex spineVertex = new HalfEdgeVertex(calcCenterPoint(start));
        spineVertex.setHalfEgde(start);
        skeletonVertices.add(spineVertex);
    }

    /**
     * Calculate center point of a given half edge
     *
     * @param HE Half edge of half edge mesh
     * @return Center point of half edge
     */
    private Vector calcCenterPoint(HalfEdge HE) {
        Vector a = HE.getStartVertex().getPosition();
        Vector b = HE.getEndVertex().getPosition();
        Vector centerPoint = a.add(b).multiply(0.5);

        return centerPoint;
    }

    public ArrayList<HalfEdgeVertex> getSkeletonVertices() {
        return skeletonVertices;
    }
}
