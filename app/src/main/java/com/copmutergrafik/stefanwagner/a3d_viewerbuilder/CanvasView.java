package com.copmutergrafik.stefanwagner.a3d_viewerbuilder;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.builder.Triangulation;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.rendering.Pixel;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.math.MathHelpers.Vector;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.scenegraph.ContourTestNode;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.scenegraph.TriangleRootNode;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.utilities.ProgressDialogTask;

import java.util.ArrayList;

/**
 * Created by Stefan on 04.02.2017.
 */

public class CanvasView extends View {
    public int width;
    public int height;

    private Bitmap mBitmap;
    private Canvas mCanvas;

    private Path mPath;
    private Paint mPaint;

    private Context context;

    private ProgressDialogTask task;

    private float mX, mY;
    private static final float TOLERANCE = 5.0f;

    public CanvasView(Context context, AttributeSet attrs) {
        super(context, attrs);

        Pixel.clear();

        this.context = context;

        this.setDrawingCacheEnabled(true);
        this.buildDrawingCache(true);

        mPath = new Path();
        mPaint = new Paint();

        mPaint.setAntiAlias(true);
        mPaint.setColor(Color.GREEN);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeJoin((Paint.Join.ROUND));
        mPaint.setStrokeWidth(1.0f);
    }

    /**
     * Draw painted bitmap on canvas
     *
     * @param canvas
     */
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        mCanvas.drawPath(mPath, mPaint);
        canvas.drawBitmap(mBitmap, 0, 0, mPaint);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        mCanvas = new Canvas(mBitmap);
    }

    /**
     * REgister touch when first touched.
     * @param x
     * @param y
     */
    private void startTouch(float x, float y) {
        mPath.moveTo(x, y);
        Pixel.getDrawnPoints().add(new Pixel((int) x, (int) y));
        mX = x;
        mY = y;
    }

    /**
     * Register touch while moving.
     * @param x
     * @param y
     */
    private void moveTouch(float x, float y) {
        float dx = Math.abs(x - mX);
        float dy = Math.abs(y - mY);

        if (dx >= TOLERANCE || dy >= TOLERANCE) {
            mPath.lineTo(mX, mY);
            Pixel.getDrawnPoints().add(new Pixel((int) mX, (int) mY));
            mX = x;
            mY = y;
        }
    }

    /**
     * Register touch when touch finished.
     */
    private void upTouch() {
        mPath.lineTo(mX, mY);
        Pixel.getDrawnPoints().add(new Pixel((int) mX, (int) mY));
        mPath.close();

    }

    /**
     * Touch Event
     *
     */
    public boolean onTouchEvent(MotionEvent event) {
        float x = event.getX();
        float y = event.getY();

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                startTouch(x, y);
                invalidate();
                break;
            case MotionEvent.ACTION_MOVE:
                moveTouch(x, y);
                invalidate();
                break;
            case MotionEvent.ACTION_UP:
                upTouch();
                invalidate();
                break;
        }
        return true;
    }

    /**
     * Create the 3D-Object "Make it 3D"
     */
    public void create3DObject() {
        task = new ProgressDialogTask(context);
        task.execute();
    }

    /**
     * Get task to get information about running object
     * @return A ProgressDialogTask Object
     */
    public ProgressDialogTask getTask() {
        return task;
    }
}
