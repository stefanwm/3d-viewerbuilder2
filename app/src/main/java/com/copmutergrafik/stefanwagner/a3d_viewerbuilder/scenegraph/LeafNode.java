package com.copmutergrafik.stefanwagner.a3d_viewerbuilder.scenegraph;

import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.math.MathHelpers.Matrix;

/**
 * Created by Stefan Wagner on 30.01.2017.
 */

public abstract class LeafNode extends INode {

    @Override
    public void traverse(RenderMode mode, Matrix modelMatrix) {
        Camera.mModelMatrix = modelMatrix.matrixToArray();
        drawGL();
    }

    @Override
    public void timerTick(int counter) {
    }

    /**
     * Draw GL content.
     */
    public abstract void drawGL();
}
