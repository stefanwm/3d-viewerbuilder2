package com.copmutergrafik.stefanwagner.a3d_viewerbuilder;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;

import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.scenegraph.RootNode;

/**
 * Created by Stefan Wagner on 30.01.2017.
 * This class provides a view to render an object.
 */
public class ViewerGLSurfaceView extends GLSurfaceView {
    private final ViewerGLRenderer mRenderer;
    private ScaleGestureDetector mScaleDetector;
    private RootNode root;

    private final float TOUCH_SCALE_FACTOR = 0.01f;
    private float mPreviousX;
    private float mPreviousY;

    public ViewerGLSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setEGLContextClientVersion(2);

        this.root = ViewerActivity.root;
        mRenderer = new ViewerGLRenderer(root);
        mScaleDetector = new ScaleGestureDetector(context, new ScaleListener());

        // Set the Renderer for drawing on the GLSurfaceView
        setRenderer(mRenderer);
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        // MotionEvent reports input details from the touch screen
        // and other input controls. In this case, you are only
        // interested in events where the touch position changed.
        float x = 0.0f;
        float y = 0.0f;

        x = e.getX();
        y = e.getY();

        mScaleDetector.onTouchEvent(e);

        switch (e.getAction()) {
            case MotionEvent.ACTION_MOVE:
                //Log.e("TOUCH", "Touch registered");
                if (mPreviousX > 0 && mPreviousY > 0) {
                    float dx = x - mPreviousX;
                    float dy = y - mPreviousY;
                    root.getCamera().updateExtrinsicCameraParams(dx * TOUCH_SCALE_FACTOR, dy * TOUCH_SCALE_FACTOR);
                }
                mPreviousX = x;
                mPreviousY = y;
        }
        requestRender();
        return true;
    }

    private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            float factor = 0;

            if (detector.getScaleFactor() < 1) {
                factor = (detector.getScaleFactor() - 1) * 100;
            } else {
                factor = detector.getScaleFactor();
            }

            root.getCamera().zoom(factor);
            invalidate();
            return true;
        }
    }
}
