package com.copmutergrafik.stefanwagner.a3d_viewerbuilder.utilities;

import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.math.MathHelpers.MathHelpers;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.mesh.HalfEdgeTriangleMesh;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.math.MathHelpers.Vector;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.scenegraph.InnerNode;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.scenegraph.LeafNode;


/**
 * Created by Stefan on 27.02.2017.
 * This class centers an object to the view point of the camera
 */

public class ObjectCenter extends InnerNode {
    private HalfEdgeTriangleMesh mesh;

    public static Vector p = new Vector(3);
    public static float fDistance = 0.0f;

    public static void centerObject(HalfEdgeTriangleMesh mesh) {
        Vector maxXYZ = findMax(mesh);
        Vector minXYZ = findMin(mesh);

        p.set(0, (minXYZ.x() + maxXYZ.x()) / 2.0f);
        p.set(1, (minXYZ.y() + maxXYZ.y()) / 2.0f);
        p.set(2, (minXYZ.z() + maxXYZ.z()) / 2.0f);

        float r = (float) Math.sqrt(Math.pow(maxXYZ.x() - p.x(), 2) + Math.pow(maxXYZ.y() - p.y(), 2) + Math.pow(maxXYZ.z() - p.z(), 2));

        fDistance = r / 0.57735f;
    }

    /**
     * Find max vertex in 3D-Object
     *
     * @param mesh Mesh where point is to be found
     * @return Max point in mesh of 3D-Object
     */
    private static Vector findMax(HalfEdgeTriangleMesh mesh) {
        Vector value = new Vector(3);
        Vector max = new Vector(3);
        for (int i = 0; i < mesh.getHalfEdgeVertices().size(); i++) {
            value = mesh.getHalfEdgeVertices().get(i).getPosition();
            if (value.x() > max.x()) {
                max.set(0, value.x());
            }
            if (value.y() > max.y()) {
                max.set(1, value.y());
            }
            if (value.z() > max.z()) {
                max.set(2, value.z());
            }
        }
        return max;
    }

    /**
     * Find min vertex in 3D-Object
     *
     * @param mesh Mesh where point is to be found
     * @return Min point in mesh of 3D-Object
     */
    private static Vector findMin(HalfEdgeTriangleMesh mesh) {
        Vector value = new Vector(3);
        Vector min = new Vector(3);
        for (int i = 0; i < mesh.getHalfEdgeVertices().size(); i++) {
            value = mesh.getHalfEdgeVertices().get(i).getPosition();
            if (value.x() < min.x()) {
                min.set(0, value.x());
            }
            if (value.y() > min.y()) {
                min.set(1, value.y());
            }
            if (value.z() > min.z()) {
                min.set(2, value.z());
            }
        }
        return min;
    }


}
