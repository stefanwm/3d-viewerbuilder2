package com.copmutergrafik.stefanwagner.a3d_viewerbuilder.scenegraph;

import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.math.MathHelpers.Matrix;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.math.MathHelpers.Vector;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Stefan Wagner on 30.01.2017.
 */

public class InnerNode extends INode {
    /**
     * List of child nodes.
     */
    private List<INode> children = new ArrayList<INode>();

    @Override
    public void traverse(RenderMode mode, Matrix modelMatrix) {
        for (INode child : children) {
            child.traverse(mode, modelMatrix);
        }
    }
    @Override
    public void timerTick(int counter) {
        for (INode child : children) {
            child.timerTick(counter);
        }
    }
    /**
     * Add new child node.
     **/
    public void addChild(INode child) {
        child.setParentNode(this);
        children.add(child);
    }

    public void removeChild(INode child){
        children.remove(child);
    }
}
