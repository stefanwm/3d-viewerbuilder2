package com.copmutergrafik.stefanwagner.a3d_viewerbuilder.halfedge;

import java.io.Serializable;

/**
 * Created by Stefan on 02.02.2017.
 */

public class HalfEdge implements Serializable {

    /**
     * Reference to the next edge in the mesh.
     */
    private HalfEdge next;

    /**
     * Reference to the opposite edge in the mesh.
     */
    private HalfEdge opposite;

    /**
     * Start vertex of the half edge.
     */
    private HalfEdgeVertex startVertex;

    private HalfEdgeVertex endVertex;

    /**
     * The half edge belongs to this facet.
     */
    private HalfEdgeTriangle facet;

    public HalfEdge getNext() {
        return next;
    }

    public void setNext(HalfEdge next) {
        this.next = next;
    }

    public HalfEdge getOpposite() {
        return opposite;
    }

    public void setOpposite(HalfEdge opposite) {
        this.opposite = opposite;
    }

    public HalfEdgeVertex getStartVertex() {
        return startVertex;
    }

    public void setStartVertex(HalfEdgeVertex startVertex) {
        this.startVertex = startVertex;
    }

    public HalfEdgeVertex getEndVertex() { return endVertex; }

    public void setEndVertex(HalfEdgeVertex endVertex) { this.endVertex = endVertex; }

    public HalfEdgeTriangle getFacet() {
        return facet;
    }

    public void setFacet(HalfEdgeTriangle facet) {
        this.facet = facet;
    }

    @Override
    public String toString() {
        return "Half Edge";
    }
}
