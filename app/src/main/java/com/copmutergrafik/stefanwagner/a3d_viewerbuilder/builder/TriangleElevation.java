package com.copmutergrafik.stefanwagner.a3d_viewerbuilder.builder;

import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.halfedge.HalfEdgeVertex;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.math.MathHelpers.Vector;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Stefan on 14.02.2017.
 * This class creates elevated triangle edges which become the arc segments of the object
 */

public class TriangleElevation implements Serializable {
    private Skeleton skeleton;
    private ArrayList<ArcSegment<Vector>> segmentSectorList;
    private ArrayList<ArcSegment<Vector>> arcSegments;
    private ArrayList<Vector> arcVertices;

    private final float delta = 0.1f;

    public TriangleElevation(Skeleton skeleton) {
        this.skeleton = skeleton;
        this.segmentSectorList = new ArrayList<>();
        this.arcSegments = new ArrayList<>();
        this.arcVertices = new ArrayList<>();

        elevateTriangleEdges();
    }

    /**
     * Elevate triangle edges by calculating arc points from end point and start point to skeleton vertex
     * as maximum elevation of arc segment given a specific increment
     */
    private void elevateTriangleEdges() {
        int index = 0;
        for (int i = 0; i < skeleton.getSkeletonVertices().size(); i++) {
            HalfEdgeVertex spineVertex = skeleton.getSkeletonVertices().get(i);

            ArcSegment<Vector> arcSegment = new ArcSegment(spineVertex);
            ArrayList<Vector> preliminaryArcSegment1 = new ArrayList<>();
            ArrayList<Vector> preliminaryArcSegment2 = new ArrayList<>();

            Vector O = new Vector(spineVertex.getPosition().x(), spineVertex.getPosition().y(), 0);
            Vector A = spineVertex.getHalfEdge().getStartVertex().getPosition();
            Vector B = spineVertex.getPosition();

            for (float s = 0; s <= 1; s += delta) {
                Vector AO = A.subtract(O);
                Vector BO = B.subtract(O);

                float radius = calculateRadius(AO);

                Vector P = calculateP(AO, BO, s);
                Vector u = P.getNormalized().multiply(radius);
                Vector x = O.add(u);

                x.setId(String.valueOf(index));
                preliminaryArcSegment1.add(x);
                arcVertices.add(x);
                index++;
            }

            A = spineVertex.getHalfEdge().getEndVertex().getPosition();


            for (float s = 0; s <= 1; s += delta) {

                Vector AO = A.subtract(O);
                Vector BO = B.subtract(O);

                float radius = calculateRadius(AO);

                Vector P = calculateP(AO, BO, s);
                Vector u = P.getNormalized().multiply(radius);
                Vector x = O.add(u);

                x.setId(String.valueOf(index));
                preliminaryArcSegment2.add(x);
                arcVertices.add(x);
                index++;
            }

            Collections.reverse(preliminaryArcSegment2);
            arcSegment.addAll(preliminaryArcSegment1);
            arcSegment.addAll(preliminaryArcSegment2);
            arcSegments.add(arcSegment);
        }

        removeSmallSegments();
        classifySegments();
    }

    /**
     * Remove small segments below a certain threshold that interfere with creating a continous mesh
     */
    private void removeSmallSegments() {
        float sum = 0.0f;
        int noe = 0;
        for (int i = 0; i < arcSegments.size(); i++) {
            sum += ((Vector) arcSegments.get(i).get(9)).z();
            noe++;
        }

        float average = sum / noe;
        for (int i = 0; i < arcSegments.size(); i++) {
            if (((Vector) arcSegments.get(i).get(9)).z() < 0.055) {
                arcSegments.remove(i);
                i = 0;
            }
        }
    }

    /**
     * Classify all arc segments by their starting point to avoid artifacts
     */
    private void classifySegments() {
        ArrayList<String> knownIds = new ArrayList<>();
        for (ArcSegment segment : arcSegments) {
            String vertexId = segment.getSpineVertex().getHalfEdge().getStartVertex().getId();

            if (!knownIds.contains(vertexId)) {
                ArcSegment orderedSegment = new ArcSegment(segment.getSpineVertex());
                orderedSegment.add(segment);
                segmentSectorList.add(orderedSegment);
                knownIds.add(vertexId);
            } else {
                for (ArcSegment arcSegment : segmentSectorList) {
                    if (vertexId == arcSegment.getSpineVertex().getHalfEdge().getStartVertex().getId()) {
                        arcSegment.add(segment);
                    }
                }
            }
        }
    }

    /**
     * Calculate radius by which to calculate arc pooint
     *
     * @param AO
     * @return
     */
    public float calculateRadius(Vector AO) {
        return (float) AO.getNorm();
    }


    /**
     * Calculate arc position of point
     *
     * @param A Start point of arc
     * @param B End point of arc
     * @param s Increment
     * @return
     */
    public Vector calculateP(Vector A, Vector B, float s) {
        return A.add(B.subtract(A).multiply(s));
    }

    public ArrayList<Vector> getArcVertices() {
        return arcVertices;
    }

    public ArrayList<ArcSegment<Vector>> getSegmentSectorList() {
        return segmentSectorList;
    }

    public ArrayList<ArcSegment<Vector>> getArcSegments() {
        return arcSegments;
    }
}