package com.copmutergrafik.stefanwagner.a3d_viewerbuilder.builder;

import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.halfedge.HalfEdge;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.halfedge.HalfEdgeVertex;

import java.util.ArrayList;

/**
 * Created by Stefan Wagner on 16.02.2017.
 * This class represents an arc segment created from a spine vertex.
 */

public class ArcSegment<Vector> extends ArrayList {

    private HalfEdgeVertex spineVertex;

    public ArcSegment(HalfEdgeVertex spineVertex) {
        this.spineVertex = spineVertex;
    }

    public HalfEdgeVertex getSpineVertex() {
        return spineVertex;
    }
}


