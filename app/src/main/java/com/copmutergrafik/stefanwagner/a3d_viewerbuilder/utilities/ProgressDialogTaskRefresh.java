package com.copmutergrafik.stefanwagner.a3d_viewerbuilder.utilities;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.ViewerActivity;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.builder.MeshGenerator;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.builder.TriangleElevation;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.math.MathHelpers.Vector;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.mesh.HalfEdgeTriangleMesh;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.rendering.Pixel;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.scenegraph.ContourTestNode;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.scenegraph.HalfEdgeMeshNodeSimple;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.scenegraph.TranslationNode;

/**
 * Created by Stefan on 27.02.2017.
 */

public class ProgressDialogTaskRefresh extends AsyncTask<Void, String, Void>{

    private ProgressDialog dialog;
    private TriangleElevation te;
    private float meshSize;
    private int iterations;
    boolean vertexLighting;

    public ProgressDialogTaskRefresh(Context context, TriangleElevation te, float meshSize, boolean vertexLighting) {
        dialog = new ProgressDialog(context);
        this.te = te;

        this.meshSize = meshSize;
        this.iterations = iterations;
        this.vertexLighting = vertexLighting;
    }

    @Override
    protected void onPreExecute() {
        dialog.show();
    }

    @Override
    protected void onPostExecute(Void result) {
        if (dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    @Override
    protected Void doInBackground(Void... params) {
        publishProgress("Generating mesh...");
        HalfEdgeTriangleMesh tMesh2 = new HalfEdgeTriangleMesh(false);
        MeshGenerator meshGenerator = new MeshGenerator(te, tMesh2);
        meshGenerator.generateMesh(this.meshSize, this.iterations);

        publishProgress("Centering object...");
        ObjectCenter.centerObject(tMesh2);

        publishProgress("Setting up view nodes");
        HalfEdgeMeshNodeSimple hfn = new HalfEdgeMeshNodeSimple(tMesh2, vertexLighting);
        ContourTestNode ctn = new ContourTestNode(Pixel.getNormedDrawnPoints());
        TranslationNode trn = new TranslationNode(new Vector(-ObjectCenter.p.x(), -ObjectCenter.p.y(), -ObjectCenter.p.z()));
        trn.addChild(hfn);

        publishProgress("Finished");
        ViewerActivity.addToChildren(trn);

        return null;
    }

    public TriangleElevation getTriangleElevation() {
        return te;
    }

    protected void onProgressUpdate(String... message) {
        super.onProgressUpdate(message);
        Log.e("Message", message[0]);
        dialog.setMessage(message[0]);
    }
}
