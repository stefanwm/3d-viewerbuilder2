package com.copmutergrafik.stefanwagner.a3d_viewerbuilder;

import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.os.SystemClock;
import android.util.Log;

import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.math.MathHelpers.Matrix;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.math.MathHelpers.Vector;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.rendering.Lighting;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.rendering.Shader;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.rendering.ShaderAttributes;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.scenegraph.Camera;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.scenegraph.INode;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.scenegraph.RootNode;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.utilities.ObjectCenter;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;


/**
 * Created by Stefan Wagner on 30.01.2017.
 * This class renders an object into the scene. Shaders are compiled and linked
 * and Scenegraph node is traversed to draw all children nodes.
 */
public class ViewerGLRenderer implements GLSurfaceView.Renderer {
    private final RootNode root;

    public ViewerGLRenderer(RootNode root) {
        this.root = root;
    }

    public void onSurfaceCreated(GL10 unused, EGLConfig config) {
        // define the color we want to be displayed as the "clipping wall"
        GLES20.glClearColor((float) root.getBackgroundColor().x(),
                (float) root.getBackgroundColor().y(),
                (float) root.getBackgroundColor().z(), 1.0f);

        // Enable depth testing
        GLES20.glEnable(GLES20.GL_DEPTH_TEST);

        ShaderAttributes.mPerVertexProgramHandle = root.getShader().compileAndLink(new String[]{"a_Position", "a_Color", "a_Normal"});
        Shader.checkGlError();
    }

    public void onDrawFrame(GL10 unused) {
        GLES20.glClear(GLES20.GL_DEPTH_BUFFER_BIT | GLES20.GL_COLOR_BUFFER_BIT);

        //Reload shader files when changing shader mode
        if (Shader.isShaderModified()) {
            ShaderAttributes.mPerVertexProgramHandle = root.getShader().compileAndLink(new String[]{"a_Position", "a_Color", "a_Normal"});
            Shader.setShaderModified(false);
        }

        ShaderAttributes.mMVPMatrixHandle = GLES20.glGetUniformLocation(ShaderAttributes.mPerVertexProgramHandle, "u_MVPMatrix");
        ShaderAttributes.mMVMatrixHandle = GLES20.glGetUniformLocation(ShaderAttributes.mPerVertexProgramHandle, "u_MVMatrix");
        ShaderAttributes.viewMatrixHandle = GLES20.glGetUniformLocation(ShaderAttributes.mPerVertexProgramHandle, "viewMatrix");
        ShaderAttributes.modelMatrixHandle = GLES20.glGetUniformLocation(ShaderAttributes.mPerVertexProgramHandle, "modelMatrix");
        ShaderAttributes.mLightPosHandle = GLES20.glGetUniformLocation(ShaderAttributes.mPerVertexProgramHandle, "u_LightPos");
        ShaderAttributes.cameraPosHandle = GLES20.glGetUniformLocation(ShaderAttributes.mPerVertexProgramHandle, "camera_position");
        ShaderAttributes.projectionMatrixHandle = GLES20.glGetUniformLocation(ShaderAttributes.mPerVertexProgramHandle, "projection_position");
        ShaderAttributes.mPositionHandle = GLES20.glGetAttribLocation(ShaderAttributes.mPerVertexProgramHandle, "a_Position");
        ShaderAttributes.mColorHandle = GLES20.glGetAttribLocation(ShaderAttributes.mPerVertexProgramHandle, "a_Color");
        ShaderAttributes.mNormalHandle = GLES20.glGetAttribLocation(ShaderAttributes.mPerVertexProgramHandle, "a_Normal");

        //Set light matrix
        Lighting.setupLightMatrix(new Vector(0.0f, 0.0f, 0.0f));

        // Draw the triangle facing straight on.
        android.opengl.Matrix.setIdentityM(Camera.mModelMatrix, 0);
        root.getShader().use(ShaderAttributes.mPerVertexProgramHandle);
        root.traverse(INode.RenderMode.REGULAR, Matrix.createIdentity4());
    }

    public void onSurfaceChanged(GL10 unused, int width, int height) {
        GLES20.glViewport(0, 0, width, height);
        root.getCamera().setupProjectionMatrix(width, height);
    }
}
