package com.copmutergrafik.stefanwagner.a3d_viewerbuilder.rendering;

import android.opengl.GLES20;

import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.math.MathHelpers.Matrix;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.math.MathHelpers.Vector;

import static com.copmutergrafik.stefanwagner.a3d_viewerbuilder.rendering.Shader.ShaderMode.PHONG;

/**
 * Created by Stefan Wagner on 30.01.2017.
 */

public class ShaderAttributes {
    /**
     * Uniform and attribute parameter location in the shader program.
     */

    public static int mMVPMatrixHandle;

    /**
     * This will be used to pass in model position information.
     */
    public static int mPositionHandle;

    public static int viewMatrixHandle;

    public static int modelMatrixHandle;


    /**
     * This will be used to pass in the modelview matrix.
     */
    public static int mMVMatrixHandle;

    /**
     * This will be used to pass in the light position.
     */
    public static int mLightPosHandle;

    public static int projectionMatrixHandle;
    /**
     * This will be used to pass in model normal information.
     */
    public static int mNormalHandle;


    /**
     * This will be used to pass in model color information.
     */
    public static int mColorHandle;

    /**
     * This is a handle to our per-vertex cube shading program.
     */
    public static int mPerVertexProgramHandle;

    /**
     * This is a handle to our light point program.
     */
    public static int mPointProgramHandle;

    public static int pointVertexShaderHandle;

    public static int pointFragmentShaderHandle;

    /**
     * Singleton instance
     */
    private static ShaderAttributes instance = null;
    public static int cameraPosHandle;

    private ShaderAttributes() {
    }

    /**
     * Getters and Setters
     */
    public static int getPointVertexShaderHandle() {
        return pointVertexShaderHandle;
    }

    public static void setPointVertexShaderHandle(int pointVertexShaderHandle) {
        ShaderAttributes.pointVertexShaderHandle = pointVertexShaderHandle;
    }

    public static int getPointFragmentShaderHandle() {
        return pointFragmentShaderHandle;
    }

    public static void setPointFragmentShaderHandle(int pointFragmentShaderHandle) {
        ShaderAttributes.pointFragmentShaderHandle = pointFragmentShaderHandle;
    }

    /**
     * Getter for the singleton instance.
     */
    public static ShaderAttributes getInstance() {
        if (instance == null) {
            instance = new ShaderAttributes();
        }
        return instance;
    }
}
