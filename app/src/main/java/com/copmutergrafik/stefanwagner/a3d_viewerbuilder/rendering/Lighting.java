package com.copmutergrafik.stefanwagner.a3d_viewerbuilder.rendering;

import android.opengl.GLES20;
import android.util.Log;

import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.math.MathHelpers.Vector;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.scenegraph.Camera;

/**
 * Created by Stefan Wagner on 31.01.2017.
 * This class represents a lighting object
 */

public class Lighting {
    public static float[] mLightModelMatrix = new float[16];

    /**
     * Used to hold a light centered on the origin in model space. We need a 4th coordinate so we can get translations to work when
     * we multiply this by our transformation matrices.
     */
    public static final float[] mLightPosInModelSpace = new float[]{0.0f, 0.0f, 0.0f, 1.0f};

    /**
     * Used to hold the current position of the light in world space (after transformation via model matrix).
     */
    public static final float[] mLightPosInWorldSpace = new float[4];

    /**
     * Used to hold the transformed position of the light in eye space (after transformation via modelview matrix)
     */
    public static final float[] mLightPosInEyeSpace = new float[4];


    /**
     * Calculate and set position of light object
     *
     * @param position
     */
    public static void setupLightMatrix(Vector position) {
        android.opengl.Matrix.setIdentityM(mLightModelMatrix, 0);
        android.opengl.Matrix.translateM(mLightModelMatrix, 0, 0.0f, 0.0f, 2.0f);

        android.opengl.Matrix.multiplyMV(mLightPosInWorldSpace, 0, mLightModelMatrix, 0, mLightPosInModelSpace, 0);
        android.opengl.Matrix.multiplyMV(mLightPosInEyeSpace, 0, Camera.mViewMatrix, 0, mLightPosInWorldSpace, 0);
    }

    /**
     * Calculate lighting matrix for MVP matrix, get shader handles and draw lighting object
     */
    public static void drawLight() {
        final int pointMVPMatrixHandle = GLES20.glGetUniformLocation(ShaderAttributes.mPointProgramHandle, "u_MVPMatrix");
        final int pointPositionHandle = GLES20.glGetAttribLocation(ShaderAttributes.mPointProgramHandle, "a_Position");

        // Pass in the position.
        GLES20.glVertexAttrib3f(pointPositionHandle, 0.0f, 0.0f, 2.0f);

        // Since we are not using a buffer object, disable vertex arrays for this attribute.
        GLES20.glDisableVertexAttribArray(pointPositionHandle);

        // Pass in the transformation matrix.
        android.opengl.Matrix.multiplyMM(Camera.mMVPMatrix, 0, Camera.mViewMatrix, 0, mLightModelMatrix, 0);
        android.opengl.Matrix.multiplyMM(Camera.mMVPMatrix, 0, Camera.mProjectionMatrix, 0, Camera.mMVPMatrix, 0);
        GLES20.glUniformMatrix4fv(pointMVPMatrixHandle, 1, false, Camera.mMVPMatrix, 0);

        // Draw the point.
        GLES20.glDrawArrays(GLES20.GL_POINTS, 0, 1);
    }
}
