package com.copmutergrafik.stefanwagner.a3d_viewerbuilder.rendering;

import android.opengl.GLES20;
import android.opengl.Matrix;
import android.util.Log;

import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.scenegraph.Camera;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.List;

/**
 * Created by Stefan Wagner on 30.01.2017.
 */

public class VertexBufferObject {

    private List<RenderVertex> renderVertices = null;

    private int primitiveType = GLES20.GL_TRIANGLES;

    private static final int FLOAT_SIZE_IN_BYTES = 4;
    private static final int INT_SIZE_IN_BYTES = 4;

    private FloatBuffer positionBuffer = null;
    private FloatBuffer normalBuffer = null;
    private FloatBuffer colorBuffer = null;
    private IntBuffer indexBuffer = null;

    public VertexBufferObject() {
    }

    /**
     * Set the data for the Buffer. The format is described together with the
     * vertices, normals and colors attributes.
     */
    public void Setup(List<RenderVertex> renderVertices, int primitiveType) {
        this.renderVertices = renderVertices;
        this.primitiveType = primitiveType;
    }

    /**
     * Init VBO, called only once (or if the date changed).
     */
    private void init() {
        if (renderVertices == null || renderVertices.size() == 0) {
            return;
        }

        positionBuffer = createPositionBuffer();
        normalBuffer = createNormalBuffer();
        colorBuffer = createColorBuffer();
        indexBuffer = createIndexBuffer();
        Shader.checkGlError();
        System.out.println("Created VBO buffers (vertex, normal, color, index).");
    }

    /**
     * Create position buffer from data.
     */
    private FloatBuffer createPositionBuffer() {
        int dataLength = renderVertices.size() * 3 * FLOAT_SIZE_IN_BYTES;
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(dataLength);
        float[] data = new float[renderVertices.size() * 3];
        for (int i = 0; i < renderVertices.size(); i++) {
            data[i * 3] = (float) renderVertices.get(i).position.x();
            data[i * 3 + 1] = (float) renderVertices.get(i).position.y();
            data[i * 3 + 2] = (float) renderVertices.get(i).position.z();
        }
        byteBuffer.order(ByteOrder.nativeOrder());
        FloatBuffer floatBuffer = byteBuffer.asFloatBuffer();
        floatBuffer.put(data);
        floatBuffer.position(0);
        return floatBuffer;
    }

    /**
     * Create normal buffer from data.
     */
    private FloatBuffer createNormalBuffer() {
        int dataLength = renderVertices.size() * 3 * FLOAT_SIZE_IN_BYTES;
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(dataLength);
        float[] data = new float[renderVertices.size() * 3];
        for (int i = 0; i < renderVertices.size(); i++) {
            data[i * 3] = (float) renderVertices.get(i).normal.x();
            data[i * 3 + 1] = (float) renderVertices.get(i).normal.y();
            data[i * 3 + 2] = (float) renderVertices.get(i).normal.z();
        }
        byteBuffer.order(ByteOrder.nativeOrder());
        FloatBuffer floatBuffer = byteBuffer.asFloatBuffer();
        floatBuffer.put(data);
        floatBuffer.position(0);
        return floatBuffer;
    }

    /**
     * Create color buffer from data.
     */
    private FloatBuffer createColorBuffer() {
        int dataLength = renderVertices.size() * 4 * FLOAT_SIZE_IN_BYTES;
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(dataLength);
        float[] data = new float[renderVertices.size() * 4];
        for (int i = 0; i < renderVertices.size(); i++) {
            data[i * 4] = (float) renderVertices.get(i).color.x();
            data[i * 4 + 1] = (float) renderVertices.get(i).color.y();
            data[i * 4 + 2] = (float) renderVertices.get(i).color.z();
            data[i * 4 + 3] = (float) renderVertices.get(i).color.w();
        }
        byteBuffer.order(ByteOrder.nativeOrder());
        FloatBuffer floatBuffer = byteBuffer.asFloatBuffer();
        floatBuffer.put(data);
        floatBuffer.position(0);
        return floatBuffer;
    }

    private IntBuffer createIndexBuffer() {
        ByteBuffer ibb =
                ByteBuffer.allocateDirect(renderVertices.size() * INT_SIZE_IN_BYTES);
        ibb.order(ByteOrder.nativeOrder());
        IntBuffer indicesBuf = ibb.asIntBuffer();
        for (int i = 0; i < renderVertices.size(); i++) {
            indicesBuf.put(i);
        }
        indicesBuf.position(0);
        return indicesBuf;
    }

    /**
     * Draw using the VBO
     */
    public void draw() {
        //Log.e("REDRAWING", "redrawing");
        if (positionBuffer == null || normalBuffer == null || colorBuffer == null) {
            init();
        }

        GLES20.glEnableVertexAttribArray(
                ShaderAttributes.mPositionHandle);
        GLES20.glEnableVertexAttribArray(
                ShaderAttributes.mNormalHandle);
        GLES20.glEnableVertexAttribArray(
                ShaderAttributes.mColorHandle);

        try {
            GLES20.glVertexAttribPointer(
                    ShaderAttributes.mPositionHandle, 3, GLES20.GL_FLOAT,
                    false, 0, positionBuffer);
            GLES20.glVertexAttribPointer(
                    ShaderAttributes.mNormalHandle, 3, GLES20.GL_FLOAT,
                    false, 0, normalBuffer);
            GLES20.glVertexAttribPointer(
                    ShaderAttributes.mColorHandle, 4, GLES20.GL_FLOAT,
                    false, 0, colorBuffer);

            GLES20.glUniformMatrix4fv(ShaderAttributes.viewMatrixHandle, 1, false, Camera.mViewMatrix, 0);
            GLES20.glUniformMatrix4fv(ShaderAttributes.modelMatrixHandle, 1, false, Camera.mModelMatrix, 0);
            GLES20.glUniformMatrix4fv(ShaderAttributes.projectionMatrixHandle, 1, false, Camera.mProjectionMatrix, 0);
            GLES20.glUniform3f(ShaderAttributes.cameraPosHandle, (float) Camera.getEye().x(), (float) Camera.getEye().y(), (float) Camera.getEye().z());

            Matrix.multiplyMM(Camera.mMVPMatrix, 0, Camera.mViewMatrix, 0, Camera.mModelMatrix, 0);
            GLES20.glUniformMatrix4fv(ShaderAttributes.mMVMatrixHandle, 1, false, Camera.mMVPMatrix, 0);

            Matrix.multiplyMM(Camera.mMVPMatrix, 0, Camera.mProjectionMatrix, 0, Camera.mMVPMatrix, 0);
            GLES20.glUniformMatrix4fv(ShaderAttributes.mMVPMatrixHandle, 1, false, Camera.mMVPMatrix, 0);

            if (Shader.getMode() == Shader.ShaderMode.PHONG) {
                GLES20.glUniform3f(ShaderAttributes.mLightPosHandle, Lighting.mLightPosInWorldSpace[0], Lighting.mLightPosInWorldSpace[1], Lighting.mLightPosInWorldSpace[2]);
            } else if (Shader.getMode() == Shader.ShaderMode.GOURAUD) {
                GLES20.glUniform3f(ShaderAttributes.mLightPosHandle, Lighting.mLightPosInEyeSpace[0], Lighting.mLightPosInEyeSpace[1], Lighting.mLightPosInEyeSpace[2]);
            }

            GLES20.glDrawElements(primitiveType, renderVertices.size(), GLES20.GL_UNSIGNED_INT, indexBuffer);
        } catch (Exception e) {
            System.out.println("Fehler: " + e);
        }

        Shader.checkGlError();
    }

    /**
     * Delete all buffers.
     */
    public void invalidate() {
        positionBuffer = null;
        normalBuffer = null;
        colorBuffer = null;

        System.out.println("TODO: Free allocated mem.");
    }
}
