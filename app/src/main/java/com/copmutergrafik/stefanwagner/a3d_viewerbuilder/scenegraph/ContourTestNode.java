package com.copmutergrafik.stefanwagner.a3d_viewerbuilder.scenegraph;

import android.opengl.GLES20;

import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.rendering.RenderVertex;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.rendering.VertexBufferObject;
import com.copmutergrafik.stefanwagner.a3d_viewerbuilder.math.MathHelpers.Vector;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Stefan on 05.02.2017.
 */

public class ContourTestNode extends LeafNode {

    private VertexBufferObject vbo = new VertexBufferObject();

    public ContourTestNode(ArrayList<Vector> contourPoints){
        createVbo(contourPoints);
    }

    private void createVbo(ArrayList<Vector> contourPoints) {
        List<RenderVertex> renderVerticesLines = new ArrayList<RenderVertex>();
        ArrayList<Vector> points = contourPoints;

        for(int i = 0; i < contourPoints.size() - 1; i++){
            setLine(renderVerticesLines, new Vector(0.0, 0.0, -1.0), contourPoints.get(i), contourPoints.get(i+1));
        }
        setLine(renderVerticesLines, new Vector(0.0, 0.0, -1.0), contourPoints.get(contourPoints.size() - 1), contourPoints.get(0));
        vbo.Setup(renderVerticesLines, GLES20.GL_LINES);

    }

    private void setLine(List<RenderVertex> renderVerticesLines, Vector normal, Vector v0, Vector v1){
        renderVerticesLines.add(new RenderVertex(v0, normal, new Vector(1, 1.0, 1, 1)));
        renderVerticesLines.add(new RenderVertex(v1, normal, new Vector(1, 1.0, 1, 1)));
    }
    @Override
    public void drawGL() {
        vbo.draw();
    }
}
